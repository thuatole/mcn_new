package com.example.mcn;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import com.aphidmobile.flip.FlipViewController;
import com.example.mcn.AppSettings.API;
import com.example.mcn.adpater.MainActivityAdapter;
import com.example.mcn.model.Category;
import com.example.mcn.model.CategoryList;
import com.example.utilities.AppSharedPreferenceUtil;
import com.example.utilities.ConnectionDetector;
import com.google.gson.Gson;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class MainActivity extends FragmentActivity implements
		OnItemClickListener {
	
	private static final int NUMBER_FIRST_PAGE_ITEM = 3;
	private static final int NUMBER_PAGE_ITEM = 6;
	
	private SlidingMenu slidingMenu;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private ActionBar actionBar;
	private FlipViewController flipView;
	private ListView listMenu;
	public LinearLayout lnTitle;
	private MainActivityAdapter adapter;
	private ProgressDialog mDialog;
	private TextView userName, btnLogOut;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		
		final boolean check_internet = MainActivity.this.checkInternetStatus();
		
		if(check_internet == false)
		{
			Intent mIntent = new Intent(this, CheckConnection.class);
			mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			this.startActivity(mIntent);
		}
		
		flipView = new FlipViewController(this);

		// Use RGB_565 can reduce peak memory usage on large screen device, but
		// it's up to you to choose the best bitmap format
		flipView.setAnimationBitmapFormat(Bitmap.Config.RGB_565);

		setContentView(flipView);

		// add left slide menu
		slidingMenu = new SlidingMenu(this);
		slidingMenu.setMode(SlidingMenu.LEFT);
		// nếu là TOUCHMODE_FULLSCREEN sẽ ko click dc trong menu
		slidingMenu.setTouchModeBehind(SlidingMenu.TOUCHMODE_MARGIN);
		// slidingMenu.setShadowDrawable(R.drawable.actionbar_gradient);
		slidingMenu.setShadowWidth(30);
		slidingMenu.setFadeDegree(0.0f);
		slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);

		slidingMenu.setBehindWidth(650);
		slidingMenu.setMenu(R.layout.sliding_menu);

		lnTitle = (LinearLayout) findViewById(R.id.lnTitle);
		userName = (TextView)findViewById(R.id.userName);
		btnLogOut = (TextView)findViewById(R.id.log_out);
		
		actionBar = getActionBar();
		// actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setIcon(R.drawable.icon);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setCustomView(R.layout.action_bar_title_layout);
		
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.slidingMenu);
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer icon to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description */
		R.string.drawer_close /* "close drawer" description */
		) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				// getActionBar().setTitle(mTitle);
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				// getActionBar().setTitle(mDrawerTitle);
			}
		};

		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		// adapter sliding menu
		listMenu = (ListView) findViewById(R.id.listMenu);
		listMenu.setAdapter(new adapter(this));
		listMenu.setOnItemClickListener(this);

		lnTitle.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent userInfo = new Intent(MainActivity.this,
						UserInfoActivity.class);
				startActivity(userInfo);

			}
		});
		
		btnLogOut.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				Editor editor = sp.edit();
				editor.putBoolean("login_status", false);
				editor.commit();
				Intent i = getBaseContext().getPackageManager()
			             .getLaunchIntentForPackage( getBaseContext().getPackageName() );
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
		});

		AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
				this);
		// Gọi api lấy dữ liệu category list
		callApiCategoryList(appPreference
				.getString(AppSettings.PREFERENCE_AUTH_KEY));
		
		callGetUserInfo(appPreference
				.getString(AppSettings.PREFERENCE_AUTH_KEY));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
//
//		// Init search manager
//		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
//				.getActionView();
//		searchView.setSearchableInfo(searchManager
//				.getSearchableInfo(getComponentName()));
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// onBackPressed();
			slidingMenu.toggle();
			return true;
		case R.id.action_search:
			Intent searchIntent = new Intent(getApplicationContext(), SearchActivity.class);
			startActivity(searchIntent);
			return true;
		case R.id.action_refresh:
			AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
					this);
			// Gọi api lấy dữ liệu category list
			callApiCategoryList(appPreference
					.getString(AppSettings.PREFERENCE_AUTH_KEY));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onResume() {
		super.onResume();
		flipView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		flipView.onPause();
	}

	// sliding menu

	// list menu
	class singleRow {
		String menu_name;
		int image;

		public singleRow(String menu_name, int image) {
			// TODO Auto-generated constructor stub
			this.menu_name = menu_name;
			this.image = image;

		}

	}

	class adapter extends BaseAdapter {

		ArrayList<singleRow> list;
		Context context;

		public adapter(Context c) {
			context = c;
			list = new ArrayList<singleRow>();

			Resources res = c.getResources();
			String[] menu_name = res.getStringArray(R.array.menu_name);

			int[] images = { 
					R.drawable.icon_gantoi, R.drawable.icon_barcode, R.drawable.icon_bookmark,
					 R.drawable.icon_thongtin};
			for (int i = 0; i < 4; i++) {
				singleRow s = new singleRow(menu_name[i], images[i]);
				list.add(s);
			}
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();

		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);

		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;

		}

		@SuppressLint("ViewHolder") @Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View row = inflater.inflate(R.layout.single_row_menu, listMenu,
					false);

			TextView menu_name = (TextView) row.findViewById(R.id.tvMenu);
			ImageView image = (ImageView) row.findViewById(R.id.imMenu);

			singleRow temp = list.get(position);
			menu_name.setText(temp.menu_name);
			image.setImageResource(temp.image);
			return row;

		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		// Toast.makeText(getApplicationContext(), "You clicked on position : "
		// + position + " and id : " + id, Toast.LENGTH_LONG).show();
		switch (position) {
		case 0:
			// Intent home = new Intent("com.jvit.findvoucher.MAINACTIVITY");
			// startActivity(home);
			Intent map = new Intent(this, NearMeActivity.class);
			startActivity(map);
	
			 break;
		case 1:
			Intent barcodeScreen = new Intent(this, BarcodeActivity.class);
			startActivity(barcodeScreen);
			break;
		case 2:
			Intent bookmark = new Intent(this, BookmarkActivity.class);
			startActivity(bookmark);
			break;
		case 3:
			Intent tutorial = new Intent(this, TutorialActivity.class);
			startActivity(tutorial);
			break;
		case 4:

			break;
		case 5:
			// share app
			break;
		case 6:
			break;
		default:
			break;
		}

	}

	/**
	 * Lưu image vào SDCard
	 * 
	 * @param bitmap
	 * @param name
	 */
	private void saveToSDCard(Bitmap bitmap, String name) {
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			mExternalStorageAvailable = mExternalStorageWriteable = true;
			Log.v("TAG", "SD Card is available for read and write "
					+ mExternalStorageAvailable + mExternalStorageWriteable);
			saveFile(bitmap, name);
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
			Log.v("TAG", "SD Card is available for read "
					+ mExternalStorageAvailable);
		} else {
			mExternalStorageAvailable = mExternalStorageWriteable = false;
			Log.v("TAG", "Please insert a SD Card to save your Video "
					+ mExternalStorageAvailable + mExternalStorageWriteable);
		}
	}

	/**
	 * Lưu file
	 * 
	 * @param bitmap
	 * @param name
	 */
	private void saveFile(Bitmap bitmap, String name) {
		String filename = name;
		ContentValues values = new ContentValues();
		File sdImageMainDirectory = new File(
				Environment.getExternalStorageDirectory(),
				AppSettings.FOLDER_IMAGE_APP);
		sdImageMainDirectory.mkdirs();
		File outputFile = new File(sdImageMainDirectory, filename);
		values.put(MediaStore.MediaColumns.DATA, outputFile.toString());
		values.put(MediaStore.MediaColumns.TITLE, filename);
		values.put(MediaStore.MediaColumns.DATE_ADDED,
				System.currentTimeMillis());
		values.put(MediaStore.MediaColumns.MIME_TYPE, "image/png");
		Uri uri = this.getContentResolver().insert(
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,

				values);
		try {
			OutputStream outStream = this.getContentResolver()
					.openOutputStream(uri);
			bitmap.compress(Bitmap.CompressFormat.PNG, 95, outStream);

			outStream.flush();
			outStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Kiểm tra tồn tại file
	 * 
	 * @param name
	 * @return
	 */
	private boolean hasExternalStoragePublicPicture(String name) {
		File sdImageMainDirectory = new File(
				Environment.getExternalStorageDirectory(),
				AppSettings.FOLDER_IMAGE_APP);
		File file = new File(sdImageMainDirectory, name);
		return file.exists();
	}

	private void showProcessDialog() {
		if (mDialog == null)
			mDialog = new ProgressDialog(this);
		mDialog.setMessage("Loading...");
		mDialog.setCancelable(false);
		if (!mDialog.isShowing()) {
			mDialog.show();
		}
	}

	private void dismissProcessDialog() {
		if (mDialog == null)
			mDialog = new ProgressDialog(this);
		if (mDialog.isShowing()) {
			mDialog.dismiss();
		}
	}
	
	private void callGetUserInfo(String authKey)
	{
		HttpRequestApi reAPI = new HttpRequestApi(getApplicationContext(),
				API.DATA_USER);
		reAPI.addParam("auth_key", authKey);
		// Log.i("load API", list.size() + "ss1");
		reAPI.execute(new HttpRequestListener<JSONObject>() {
			// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
				
				
			}

			// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
			@Override
			public void onPostExecute(JSONObject result) {
				// TODO Auto-generated method stub
				try {
					// System.out.println("====");
					// System.out.println(result.get("dealDetail"));

					
					String name = result.getString("name");
			
					AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(MainActivity.this);
					appPreference.putString("user_name", name);
					userName.setText(name);
					
					// Log.i("load", jsonDeals + "");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					
				}

				// Toast.makeText(getApplicationContext(), ""+result.toString(),
				// Toast.LENGTH_LONG).show();
			}
		}); // END reAPI.execute
	}

	/**
	 * Gọi api lấy danh sách category api/category/list
	 * 
	 * @param authKey
	 */
	private void callApiCategoryList(String authKey) {
		showProcessDialog();

		HttpRequestApi reAPI = new HttpRequestApi(getApplicationContext(),
				API.DATA_CATEGORY_LIST);

		reAPI.addParam("auth_key", authKey);
		reAPI.execute(new HttpRequestListener<JSONObject>() {
			@Override
			public void onPreExecute() {

			}

			@Override
			public void onPostExecute(JSONObject result) {
				try {
					if (result != null) {
						Gson gson = new Gson();
						CategoryList apiCategoryList = gson.fromJson(
								result.toString(), CategoryList.class);
						new DownloadTask().execute(apiCategoryList);
					} else {
						dismissProcessDialog();
					}
				} catch (Exception e) {
					dismissProcessDialog();
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Download dữ liệu và image
	 */
	private class DownloadTask extends
			AsyncTask<CategoryList, Integer, List<List<Category>>> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProcessDialog();
		}

		protected List<List<Category>> doInBackground(CategoryList... params) {
			CategoryList apiCategoryList = params[0];
			// List dùng để phân trang
			List<List<Category>> parentCategoryList = new ArrayList<List<Category>>();
			if (apiCategoryList != null
					&& apiCategoryList.getCategories() != null
					&& apiCategoryList.getCategories().size() > 0) {
				// Dữ liệu category sau khi lấy về từ api
				List<Category> categoryList = apiCategoryList.getCategories();
				// List quản lý cho mỗi trang tối đa 6 item
				List<Category> subCategoryList = new ArrayList<Category>();

				boolean isFirst = false;
				
				// Duyệt danh sách category
				for (int i = 0; i < categoryList.size(); i++) {
					// Lấy từng category
					Category category = categoryList.get(i);
					// Add category vào list mới để phân trang
					if (category != null) {
						subCategoryList.add(category);
						if(!isFirst) {
							if (subCategoryList.size() == NUMBER_FIRST_PAGE_ITEM) {
								parentCategoryList.add(subCategoryList);
								subCategoryList = new ArrayList<Category>();
								isFirst = true;
							} else if (i == categoryList.size() - 1) {
								if (subCategoryList.size() > 0) {
									parentCategoryList.add(subCategoryList);
								}
							}
						} else {
							if (subCategoryList.size() == NUMBER_PAGE_ITEM) {
								parentCategoryList.add(subCategoryList);
								subCategoryList = new ArrayList<Category>();
							} else if (i == categoryList.size() - 1) {
								if (subCategoryList.size() > 0) {
									parentCategoryList.add(subCategoryList);
								}
							}
						}
						
					}
					String urlImage = category.getThumb();
					String name = urlImage
							.substring(urlImage.lastIndexOf("/") + 1);
					if (!hasExternalStoragePublicPicture(name)) {
						try {
							URL url = new URL(urlImage);
							HttpURLConnection connection = (HttpURLConnection) url
									.openConnection();
							int length = connection.getContentLength();
							InputStream is = (InputStream) url.getContent();
							byte[] imageData = new byte[length];
							int buffersize = (int) Math.ceil(length
									/ (double) 100);
							int downloaded = 0;
							int read;
							while (downloaded < length) {
								if (length < buffersize) {
									read = is.read(imageData, downloaded,
											length);
								} else if ((length - downloaded) <= buffersize) {
									read = is.read(imageData, downloaded,
											length - downloaded);
								} else {
									read = is.read(imageData, downloaded,
											buffersize);
								}
								downloaded += read;
							}
							Bitmap bitmap = BitmapFactory.decodeByteArray(
									imageData, 0, length);
							if (bitmap != null) {
								Log.i("TAG", "Bitmap created");
							} else {
								Log.i("TAG", "Bitmap not created");
							}
							is.close();

							if (bitmap != null) {
								saveToSDCard(bitmap, name);
							}
						} catch (Exception e) {
							Log.e("TAG",
									"Download image error: " + e.getMessage());
							e.printStackTrace();
						}
					}
				}
			}
			return parentCategoryList;
		}

		protected void onPostExecute(List<List<Category>> result) {
			// Xét adapter cho flipView
			adapter = new MainActivityAdapter(MainActivity.this);
			adapter.setCategoryList(result);
			flipView.setAdapter(adapter);
			// dismiss dialog
			dismissProcessDialog();
		}
	}
	public boolean checkInternetStatus() { // TODO Auto-generated method stub
		boolean a = true;
		ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
		boolean isConnecting = cd.isConnectingToInternet();
		if (!isConnecting) {
			// showAlertDialog(Splash.this.getApplicationContext(),
			// "No Internet Connection",
			// "You don't have internet connection.", false);
			a = false;
		}
		return a;
	}

	@Override
	protected void onResumeFragments() {
		// TODO Auto-generated method stub
		super.onResumeFragments();
		AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(MainActivity.this);
		String name = appPreference.getString("user_name");
		userName.setText(name);
	}
	
	
}
