package com.example.mcn;

import org.json.JSONObject;

import com.example.mcn.AppSettings.API;
import com.example.utilities.AppSharedPreferenceUtil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class UserEdit extends Activity {

	
	private EditText tvName, tvNickName, tvEmail, tvPhone, tvDOB, tvAddress;
	private ProgressDialog mDialog;
	private Button btnUpdate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_edit);
		
		tvName = (EditText)findViewById(R.id.tvName);
		tvNickName = (EditText)findViewById(R.id.tvNickname);
		tvEmail = (EditText)findViewById(R.id.tvEmail);
		tvPhone = (EditText)findViewById(R.id.tvPhone);
		tvDOB = (EditText)findViewById(R.id.tvDOB);
		tvAddress = (EditText)findViewById(R.id.tvAddress);
		btnUpdate = (Button)findViewById(R.id.btnEdit);
				
		// Show process dialog
		showProcessDialog();
		
		HttpRequestApi reAPI = new HttpRequestApi(getApplicationContext(),
				API.DATA_USER);
		AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
				this);
		reAPI.addParam("auth_key",appPreference
				.getString(AppSettings.PREFERENCE_AUTH_KEY));
		// Log.i("load API", list.size() + "ss1");
		reAPI.execute(new HttpRequestListener<JSONObject>() {
			// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
				
				
			}

			// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
			@Override
			public void onPostExecute(JSONObject result) {
				// TODO Auto-generated method stub
				try {
					// System.out.println("====");
					// System.out.println(result.get("dealDetail"));

					String name = result.getString("name");
					tvName.setText(name);
					
					String nickname = result.getString("name");
					tvNickName.setText(nickname);
					
					
					String phone = result.getString("phone");
					tvPhone.setText(phone);
					
					String email = result.getString("email");
					tvEmail.setText(email);
					
					String district = result.getString("district");
					tvDOB.setText(district);
					
					String address = result.getString("address");
					tvAddress.setText(address);
					
					// Hide process dialog 
					dismissProcessDialog();
				} catch (Exception e) {
					e.printStackTrace();
					// Hide process dialog
					dismissProcessDialog();
				}
			}
		}); // END reAPI.execute
		
		
		btnUpdate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				HttpRequestApi reAPI = new HttpRequestApi(getApplicationContext(),
						API.DATA_USER);
				AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
						UserEdit.this);
				reAPI.addParam("auth_key",appPreference
						.getString(AppSettings.PREFERENCE_AUTH_KEY));
				if(tvName.length() > 0)
				{
					reAPI.addParam("name",tvName.getText().toString());
				}
				if(tvEmail.length() > 0)
				{
					
					reAPI.addParam("email",tvEmail.getText().toString());
				}
				if(tvPhone.length() > 0)
				{
					
					reAPI.addParam("phone",tvPhone.getText().toString());
				}
				if(tvDOB.length() > 0)
				{
					
					reAPI.addParam("district",tvDOB.getText().toString());
				}
				if(tvAddress.length() > 0)
				{
					
					reAPI.addParam("address",tvAddress.getText().toString());
				}
				// Log.i("load API", list.size() + "ss1");
				reAPI.execute(new HttpRequestListener<JSONObject>() {
					// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						showProcessDialog();
						
					}

					// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
					@Override
					public void onPostExecute(JSONObject result) {
						// TODO Auto-generated method stub
						try {
							// System.out.println("====");
							// System.out.println(result.get("dealDetail"));

							String name = result.getString("name");
							AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(UserEdit.this);
							appPreference.putString("user_name", name);
//							tvName.setText(name);
//							
//							String nickname = result.getString("name");
//							tvNickName.setText(nickname);
//							
//							
//							String phone = result.getString("phone");
//							tvPhone.setText(phone);
//							
//							String email = result.getString("email");
//							tvEmail.setText(email);
//							
//							String district = result.getString("district");
//							tvDOB.setText(district);
//							
//							String address = result.getString("address");
//							tvAddress.setText(address);
							AppSettings.CHECK_UPDATE_INFO = 1 ;
							UserEdit.this.onBackPressed();
							// Hide process dialog 
							//dismissProcessDialog();
						} catch (Exception e) {
							e.printStackTrace();
							// Hide process dialog
							//dismissProcessDialog();
						}
					}
				}); // END reAPI.execute
			}
		});
	}

	private void showProcessDialog() {
		if (mDialog == null)
			mDialog = new ProgressDialog(this);
		mDialog.setMessage("Loading...");
		mDialog.setCancelable(false);
		if (!mDialog.isShowing()) {
			mDialog.show();
		}
	}

	private void dismissProcessDialog() {
		if (mDialog == null)
			mDialog = new ProgressDialog(this);
		if (mDialog.isShowing()) {
			mDialog.dismiss();
		}
	}
	
}
