package com.example.mcn;

import com.example.mcn.AppSettings.API;

public class HelperUtils {

	/**
	 * Build api uri
	 * 
	 * @param enumAPI
	 * @return
	 */
	public static String getAPIUri(API enumAPI) {
		String uri = null;

		switch (enumAPI) {
		case DATA_DETAIL:
			uri = AppSettings.API_SERVER + AppSettings.API_DATA_DETAIL;
			break;
		case REG_DEVICE:
			uri = AppSettings.API_SERVER + AppSettings.API_REG_DEVICE;
			break;
		case DATA_NEAR:
			uri = AppSettings.API_SERVER + AppSettings.API_DATA_NEAR;
			break;
		case DATA_CATEGORY_LIST:
			uri = AppSettings.API_SERVER + AppSettings.API_DATA_CATEGORY_LIST;
			break;
		case DATA_USER:
			uri = AppSettings.API_SERVER + AppSettings.API_DATA_USER;
			break;
		case CARD_INFO:
			uri = AppSettings.API_SERVER + AppSettings.API_CARD_INFO;
			break;
		case DATA_MONTH:
			uri = AppSettings.API_SERVER + AppSettings.API_DATA_MONTH;
			break;
		case DATA_BARCODE:
			uri = AppSettings.API_SERVER + AppSettings.API_BARCODE;
			break;
		case DATA_PRODUCTS:
			uri = AppSettings.API_SERVER + AppSettings.API_DATA_PRODUCTS;
			break;
		case DATA_SEARCH:
			uri = AppSettings.API_SERVER + AppSettings.API_DATA_SEARCH;
			break;
		case GET_COMMENT:
			uri = AppSettings.API_SERVER + AppSettings.API_GET_COMMENT;
			break;
		case POST_COMMENT:
			uri = AppSettings.API_SERVER + AppSettings.API_POST_COMMENT;
			break;
		case CARD_RENEW:
			uri = AppSettings.API_SERVER + AppSettings.API_CARD_RENEW;
			break;
		default:
			break;
		}
		return uri;
	}

}
