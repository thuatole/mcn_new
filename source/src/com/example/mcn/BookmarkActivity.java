package com.example.mcn;

import java.util.List;

import com.example.mcn.adpater.BookmarkDBAdapter;
import com.example.mcn.model.Bookmark;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class BookmarkActivity extends ListActivity {

	private List<Bookmark> bookmarkList;
	private BookmarkAdapter bookmarkAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bookmark);

		BookmarkDBAdapter bookmarkDB = new BookmarkDBAdapter(this);
		bookmarkList = bookmarkDB.getBookmarkList();
		
		bookmarkAdapter = new BookmarkAdapter(this);
		setListAdapter(bookmarkAdapter);
	}

	@Override
	protected void onListItemClick(ListView listview, View view, int position,
			long id) {
		Intent detailProduct = new Intent(this, DetailProduct.class);
		detailProduct.putExtra("product_id",
				bookmarkAdapter.getProductId(position));
		this.startActivity(detailProduct);
	}

	public class BookmarkAdapter extends BaseAdapter {

		private LayoutInflater inflater;

		public BookmarkAdapter(Context context) {
			inflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			return bookmarkList.size();
		}

		@Override
		public Object getItem(int position) {
			return bookmarkList.get(position);
		}

		public String getProductId(int position) {
			return bookmarkList.get(position).getProduct_id();
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			TextView tvNo = null, tvName = null;
			if (convertView == null) {
				view = inflater.inflate(R.layout.bookmark_item, null);
			}

			tvNo = (TextView) view.findViewById(R.id.tvNo);
			tvName = (TextView) view.findViewById(R.id.tvName);
			Bookmark bookmark = bookmarkList.get(position);
			tvNo.setText(position + 1 + "");
			if (bookmark != null) {
				tvName.setText(bookmark.getProduct_name());
			}
			return view;
		}

	}
}
