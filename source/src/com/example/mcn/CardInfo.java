package com.example.mcn;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.mcn.AppSettings.API;
import com.example.utilities.AppSharedPreferenceUtil;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CardInfo extends Activity {

	private TextView name, serial, create_day, expire_day;
	private Button btnGiahan;
	private String period_value;
	private Spinner periodSpinner;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.card_info);
		
		name = (TextView)findViewById(R.id.tvName);
		serial = (TextView)findViewById(R.id.tvSerial);
		create_day = (TextView)findViewById(R.id.tvCreateDay);
		expire_day = (TextView)findViewById(R.id.tvExpireDay);
		btnGiahan = (Button)findViewById(R.id.btnGiahan);
		periodSpinner = (Spinner)findViewById(R.id.list_card);
		
		AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
				this);
		name.setText(appPreference
				.getString("user_name"));
		period_value = periodSpinner.getSelectedItem().toString();
		period_value = this.getCashValue(period_value);
		HttpRequestApi reAPI = new HttpRequestApi(getApplicationContext(),
				API.CARD_INFO);
		reAPI.addParam("auth_key",appPreference
				.getString(AppSettings.PREFERENCE_AUTH_KEY));
		
		// Log.i("load API", list.size() + "ss1");
		reAPI.execute(new HttpRequestListener<JSONObject>() {
			// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
				
				
			}

			// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
			@Override
			public void onPostExecute(JSONObject result) {
				// TODO Auto-generated method stub
				try {
					// System.out.println("====");
					// System.out.println(result.get("dealDetail"));

					//String name = result.getString("name");
					
					Log.i("thuat", result+"");
					JSONObject jsonDeals = (JSONObject) result
							.get("card_info");
					Log.i("thuat", jsonDeals+"");
					serial.setText(jsonDeals.getString("card_number"));
					expire_day.setText(jsonDeals.getString("expired_at"));
					create_day.setText(jsonDeals.getString("created_at"));
					// Log.i("load", jsonDeals + "");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					
				}

				// Toast.makeText(getApplicationContext(), ""+result.toString(),
				// Toast.LENGTH_LONG).show();
			}
		}); // END reAPI.execute
		
//		this.callGetCardInfo(appPreference
//				.getString(AppSettings.PREFERENCE_AUTH_KEY));
		
		periodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
		        Object item = parent.getItemAtPosition(pos);
		        period_value = item.toString();
		        period_value = CardInfo.this.getCashValue(period_value);
		    }
		    public void onNothingSelected(AdapterView<?> parent) {
		    }
		});
		
		
		btnGiahan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
						CardInfo.this);
				HttpRequestApi reAPI = new HttpRequestApi(getApplicationContext(),
						API.CARD_RENEW);
				String auth = appPreference
						.getString(AppSettings.PREFERENCE_AUTH_KEY);
				reAPI.addParam("auth_key",appPreference
						.getString(AppSettings.PREFERENCE_AUTH_KEY));
				reAPI.addParam("method",1+"");
				reAPI.addParam("months",period_value);
				// Log.i("load API", list.size() + "ss1");
				reAPI.execute(new HttpRequestListener<JSONObject>() {
					// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						
						
					}

					// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
					@Override
					public void onPostExecute(JSONObject result) {
						// TODO Auto-generated method stub
						try {
							// System.out.println("====");
							// System.out.println(result.get("dealDetail"));

							//String name = result.getString("name");
							
							if (result.getInt("code") == 0) {

								
								Toast.makeText(CardInfo.this, "Gia hạn thành công!", Toast.LENGTH_LONG).show();

							}// end if code ==1
							// Log.i("load", jsonDeals + "");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							//e.printStackTrace();
							
						}

						// Toast.makeText(getApplicationContext(), ""+result.toString(),
						// Toast.LENGTH_LONG).show();
					}
				}); // END reAPI.execute
			}
		});
	}
	
	public String getCashValue(String value)
	{
		String[] split = value.split(" ");
		return split[0];
	}

	
}
