package com.example.mcn.adpater;

import java.io.File;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mcn.AppSettings;
import com.example.mcn.ListByCate;
import com.example.mcn.R;
import com.example.mcn.model.Category;
import com.example.utilities.PhoneUtil;

public class MainActivityAdapter extends BaseAdapter {

	private Context context;
	private LayoutInflater inflater;

	private int repeatCount = 1;

	private List<List<Category>> categoryList;

	public MainActivityAdapter(Context context) {
		inflater = LayoutInflater.from(context);
		this.context = context;
	}

	@Override
	public int getCount() {
		return categoryList.size() * repeatCount;
	}

	public void setRepeatCount(int repeatCount) {
		this.repeatCount = repeatCount;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView textView1 = null, textView2 = null, textView3 = null, textView4 = null, textView5 = null, textView6 = null;
		ImageView imageView1 = null, imageView2 = null, imageView3 = null, imageView4 = null, imageView5 = null, imageView6 = null;
		RelativeLayout layout1 = null, layout2 = null, layout3 = null, layout4 = null, layout5 = null, layout6 = null;
		View view = null;

		if (position == 0) {
			view = inflater.inflate(R.layout.first_catagory_item, null);
			view.setTag("first");
		} else {
			if(convertView != null && convertView.getTag().equals("last")) {
				view = convertView;
			} else {
				view = inflater.inflate(R.layout.catagory_item, null);
				view.setTag("last");
			}
		}

		textView1 = (TextView) view.findViewById(R.id.textview_item_1);
		textView2 = (TextView) view.findViewById(R.id.textview_item_2);
		textView3 = (TextView) view.findViewById(R.id.textview_item_3);
		if (position > 0) {
			textView4 = (TextView) view.findViewById(R.id.textview_item_4);
			textView5 = (TextView) view.findViewById(R.id.textview_item_5);
			textView6 = (TextView) view.findViewById(R.id.textview_item_6);
		}

		imageView1 = (ImageView) view.findViewById(R.id.image_item_1);
		imageView2 = (ImageView) view.findViewById(R.id.image_item_2);
		imageView3 = (ImageView) view.findViewById(R.id.image_item_3);
		if (position > 0) {
			imageView4 = (ImageView) view.findViewById(R.id.image_item_4);
			imageView5 = (ImageView) view.findViewById(R.id.image_item_5);
			imageView6 = (ImageView) view.findViewById(R.id.image_item_6);
		}

		layout1 = (RelativeLayout) view.findViewById(R.id.layout_item_1);
		layout2 = (RelativeLayout) view.findViewById(R.id.layout_item_2);
		layout3 = (RelativeLayout) view.findViewById(R.id.layout_item_3);
		if (position > 0) {
			layout4 = (RelativeLayout) view.findViewById(R.id.layout_item_4);
			layout5 = (RelativeLayout) view.findViewById(R.id.layout_item_5);
			layout6 = (RelativeLayout) view.findViewById(R.id.layout_item_6);
		}

		final List<Category> subCategoryList = categoryList.get(position);
		int size = subCategoryList.size();
		String urlImage, name;
		File imgFile;
		Bitmap bitmap;
		if (size >= 1) {
			layout1.setVisibility(View.VISIBLE);
			textView1.setText(subCategoryList.get(0).getName());

			urlImage = subCategoryList.get(0).getThumb();
			name = urlImage.substring(urlImage.lastIndexOf("/") + 1);
			imgFile = new File(AppSettings.PATH_IMAGE_SDCARD + name);
			bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
			if (bitmap == null) {
				PhoneUtil.deleteImage(name);
			} else {
				imageView1.setImageBitmap(bitmap);
			}
			imageView1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					itemClick(subCategoryList.get(0));
				}
			});
		} else {
			layout1.setVisibility(View.INVISIBLE);
		}
		if (size >= 2) {
			layout2.setVisibility(View.VISIBLE);
			textView2.setText(subCategoryList.get(1).getName());

			urlImage = subCategoryList.get(1).getThumb();
			name = urlImage.substring(urlImage.lastIndexOf("/") + 1);
			imgFile = new File(AppSettings.PATH_IMAGE_SDCARD + name);
			bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

			if (bitmap == null) {
				PhoneUtil.deleteImage(name);
			} else {
				imageView2.setImageBitmap(bitmap);
			}
			imageView2.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					itemClick(subCategoryList.get(1));
				}
			});
		} else {
			layout2.setVisibility(View.INVISIBLE);
		}
		if (size >= 3) {
			layout3.setVisibility(View.VISIBLE);
			textView3.setText(subCategoryList.get(2).getName());

			urlImage = subCategoryList.get(2).getThumb();
			name = urlImage.substring(urlImage.lastIndexOf("/") + 1);
			imgFile = new File(AppSettings.PATH_IMAGE_SDCARD + name);
			bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

			if (bitmap == null) {
				PhoneUtil.deleteImage(name);
			} else {
				imageView3.setImageBitmap(bitmap);
			}
			imageView3.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					itemClick(subCategoryList.get(2));
				}
			});
		} else {
			layout3.setVisibility(View.INVISIBLE);
		}
		if (position > 0) {
			if (size >= 4) {
				layout4.setVisibility(View.VISIBLE);
				textView4.setText(subCategoryList.get(3).getName());

				urlImage = subCategoryList.get(3).getThumb();
				name = urlImage.substring(urlImage.lastIndexOf("/") + 1);
				imgFile = new File(AppSettings.PATH_IMAGE_SDCARD + name);
				bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
				if (bitmap == null) {
					PhoneUtil.deleteImage(name);
				} else {
					imageView4.setImageBitmap(bitmap);
				}

				imageView4.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						itemClick(subCategoryList.get(3));
					}
				});
			} else {
				layout4.setVisibility(View.INVISIBLE);
			}
			if (size >= 5) {
				layout5.setVisibility(View.VISIBLE);
				textView5.setText(subCategoryList.get(4).getName());

				urlImage = subCategoryList.get(4).getThumb();
				name = urlImage.substring(urlImage.lastIndexOf("/") + 1);
				imgFile = new File(AppSettings.PATH_IMAGE_SDCARD + name);
				bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
				if (bitmap == null) {
					PhoneUtil.deleteImage(name);
				} else {
					imageView5.setImageBitmap(bitmap);
				}

				imageView5.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						itemClick(subCategoryList.get(4));
					}
				});
			} else {
				layout5.setVisibility(View.INVISIBLE);
			}
			if (size >= 6) {
				layout6.setVisibility(View.VISIBLE);
				textView6.setText(subCategoryList.get(5).getName());

				urlImage = subCategoryList.get(5).getThumb();
				name = urlImage.substring(urlImage.lastIndexOf("/") + 1);
				imgFile = new File(AppSettings.PATH_IMAGE_SDCARD + name);
				bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
				if (bitmap == null) {
					PhoneUtil.deleteImage(name);
				} else {
					imageView6.setImageBitmap(bitmap);
				}

				imageView6.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						itemClick(subCategoryList.get(5));
					}
				});
			} else {
				layout6.setVisibility(View.INVISIBLE);
			}
		}

		return view;
	}

	public void setCategoryList(List<List<Category>> categoryList) {
		this.categoryList = categoryList;
	}

	private final class MyTouchListener implements OnTouchListener {
		public boolean onTouch(View view, MotionEvent motionEvent) {
			if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
				ClipData data = ClipData.newPlainText("", "");
				DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
						view);
				view.startDrag(data, shadowBuilder, view, 0);
				view.setVisibility(View.INVISIBLE);
				return true;
			} else {
				return false;
			}
		}
	}

	class MyDragListener implements OnDragListener {
		Drawable enterShape = context.getResources().getDrawable(
				R.drawable.shape_droptarget);
		Drawable normalShape = context.getResources().getDrawable(
				R.drawable.shape);

		@Override
		public boolean onDrag(View v, DragEvent event) {
			// TODO Auto-generated method stub
			int action = event.getAction();
			switch (event.getAction()) {
			case DragEvent.ACTION_DRAG_STARTED:
				// do nothing
				break;
			case DragEvent.ACTION_DRAG_ENTERED:
				v.setBackgroundDrawable(enterShape);
				break;
			case DragEvent.ACTION_DRAG_EXITED:
				v.setBackgroundDrawable(normalShape);
				break;
			case DragEvent.ACTION_DROP:
				// Dropped, reassign View to ViewGroup
				View view = (View) event.getLocalState();
				ViewGroup owner = (ViewGroup) view.getParent();
				owner.removeView(view);
				RelativeLayout container = (RelativeLayout) v;
				container.addView(view);
				view.setVisibility(View.VISIBLE);
				break;
			case DragEvent.ACTION_DRAG_ENDED:
				v.setBackgroundDrawable(normalShape);
			default:
				break;
			}
			return true;
		}
	}

	private void itemClick(Category category) {
		Intent detailCate = new Intent(context, ListByCate.class);
		detailCate.putExtra("category", category);
		context.startActivity(detailCate);
	}
}
