package com.example.mcn.adpater;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import com.example.mcn.R;
import com.example.mcn.model.Deal;
import com.example.mcn.model.ScoreInfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ScoreCollectAdapter extends BaseAdapter {

	private static ScoreCollectAdapter instance = null;

	Context context;
	private ListView listView;
	ArrayList<ScoreInfo> listScore = new ArrayList<ScoreInfo>();
	View row;
	
	public static ScoreCollectAdapter getInstance() {
		if (instance == null) {
			instance = new ScoreCollectAdapter();
		}
		return instance;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public ArrayList<ScoreInfo> getListScore() {
		return listScore;
	}

	public void setListScore(ArrayList<ScoreInfo> listScore) {
		this.listScore = listScore;
	}

	public ScoreCollectAdapter() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listScore.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listScore.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		
		row = inflater.inflate(R.layout.single_row_score, listView, false);
		
		TextView time_update = (TextView)row.findViewById(R.id.tvTimeUpdate);
		TextView detail = (TextView)row.findViewById(R.id.tvDetail);
		TextView save_amount = (TextView)row.findViewById(R.id.tvSaveAmount);
		
		final ScoreInfo temp = listScore.get(position);
		time_update.setText(temp.getTime());
		detail.setText(temp.getDetail());
		save_amount.setText(temp.getMoney_save() + " VNĐ");
		return row;
	}

	
}
