package com.example.mcn.adpater;

import java.util.ArrayList;
import com.example.mcn.R;
import com.example.mcn.model.SearchList;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SearchAdapter extends BaseAdapter {

	private static SearchAdapter instance = null;
	
	Context context;
	private ListView listView;
	ArrayList<SearchList> listSearch = new ArrayList<SearchList>();
	View row;
	
	public static SearchAdapter getInstance() {
		if (instance == null) {
			instance = new SearchAdapter();
		}
		return instance;
	}
	
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
	
	public ArrayList<SearchList> getListSearch() {
		return listSearch;
	}

	public void setListScore(ArrayList<SearchList> listSearch) {
		this.listSearch = listSearch;
	}

	public SearchAdapter() {
	}
	
	@Override
	public int getCount() {
		return listSearch.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("ViewHolder") @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		
		row = inflater.inflate(R.layout.single_row_search, listView, false);
		TextView textTitle = (TextView)row.findViewById(R.id.textAddress);
		final SearchList temp = listSearch.get(position);
		textTitle.setText(temp.getName());
		return row;
	}

}
