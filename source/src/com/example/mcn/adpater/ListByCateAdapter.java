package com.example.mcn.adpater;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mcn.AppSettings;
import com.example.mcn.DetailProduct;
import com.example.mcn.R;
import com.example.mcn.model.Product;
import com.example.utilities.PhoneUtil;

public class ListByCateAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater inflater;

	private int repeatCount = 1;

	private List<Product> listByCate;

	public ListByCateAdapter(Context context) {
		inflater = LayoutInflater.from(context);
		this.context = context;
	}

	@Override
	public int getCount() {
		return listByCate.size() * repeatCount;
	}

	public void setRepeatCount(int repeatCount) {
		this.repeatCount = repeatCount;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ImageView imgThumb = null;
		TextView tvIdPretty = null, tvPrice = null, tvAddress = null, tvTitle = null, tvDescription = null;
		Button btnDetail = null;
		if (convertView == null) {
			view = inflater.inflate(R.layout.listbycate, null);
		}

		imgThumb = (ImageView) view.findViewById(R.id.imgThumb);
		tvIdPretty = (TextView) view.findViewById(R.id.tvIdPretty);
		tvPrice = (TextView) view.findViewById(R.id.tvPrice);
		tvAddress = (TextView) view.findViewById(R.id.tvAddress);
		tvTitle = (TextView) view.findViewById(R.id.tvTitle);
		tvDescription = (TextView) view.findViewById(R.id.tvDescription);
		btnDetail = (Button) view.findViewById(R.id.btnDetail);

		final Product product = listByCate.get(position);
		if (product != null) {
			
			String name = product.getThumb().substring(product.getThumb().lastIndexOf("/") + 1);
			File imgFile = new File(AppSettings.PATH_IMAGE_SDCARD + name);
			Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
			if(bitmap != null) {
				imgThumb.setImageBitmap(bitmap);
			} else {
				PhoneUtil.deleteImage(name);
			}
			
			tvIdPretty.setText("ID:" + product.getId_pretty());

			if (product.getDiscount() != null) {
				tvPrice.setText(product.getDiscount() + "%");
			}

			if (product.getAddress() != null) {
				tvAddress.setText(product.getAddress());
			}

			if (product.getName() != null) {
				tvTitle.setText(product.getName());
			}

			if (product.getShort_description() != null) {
				tvDescription.setText(product.getShort_description());
			}
			
			imgThumb.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent detailProduct = new Intent(context,
							DetailProduct.class);
					detailProduct.putExtra("product_id", product.getId() + "");
					context.startActivity(detailProduct);
				}
			});

			btnDetail.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent detailProduct = new Intent(context,
							DetailProduct.class);
					detailProduct.putExtra("product_id", product.getId() + "");
					context.startActivity(detailProduct);
				}
			});
		}

		return view;
	}

	public List<Product> getListByCate() {
		return listByCate;
	}

	/**
	 * add danh sách vào list dành cho load more
	 */
	public void addListByCate(List<Product> listAdd) {
		// vị trí index bắt đầu add
		int begin = 0;
		if (this.listByCate != null) {
			begin = this.listByCate.size();
		}
		// size của danh sách mới muốn add
		int sizeAdd = listAdd.size();
		for (int i = begin; i < begin + sizeAdd; i++) {
			this.listByCate.add(listAdd.get(i - begin));
		}
	}

	/**
	 * reset list
	 */
	public void resetListByCate() {
		listByCate = new ArrayList<Product>();
	}
}
