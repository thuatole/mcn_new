package com.example.mcn.adpater;

import java.util.ArrayList;
import java.util.List;

import com.example.mcn.model.Bookmark;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BookmarkDBAdapter {
	public static final String TABLE_NAME = "tb_bookmark";

	public static final int DATABASE_VERSION = 1;

	public final String ID_COLUMN = "_id";
	public final String PRODUCT_ID_COLUMN = "product_id";
	public final String PRODUCT_NAME_COLUMN = "product_name";

	private static final String TABLE_CREATE = "create table " + TABLE_NAME
			+ " (_id integer primary key autoincrement, "
			+ "product_id text, product_name text);";

	private final Context context;

	private DatabaseHelper mDbHelper;

	private SQLiteDatabase mDb;

	public BookmarkDBAdapter(Context context) {
		this.context = context;
		mDbHelper = new DatabaseHelper(this.context);
	}

	private void open() throws SQLException {
		mDb = mDbHelper.getWritableDatabase();
	}

	private void close() {
		mDbHelper.close();
	}

	public List<Bookmark> getBookmarkList() {
		List<Bookmark> bookmarkList = new ArrayList<Bookmark>();
		this.open();
		Cursor cursor = mDb.query(TABLE_NAME, new String[] { PRODUCT_ID_COLUMN,
				PRODUCT_NAME_COLUMN }, null, null, null, null, null);

		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();

			Bookmark bookmark = null;
			do {
				bookmark = new Bookmark();
				bookmark.setProduct_id(cursor.getString(cursor
						.getColumnIndexOrThrow(PRODUCT_ID_COLUMN)));
				bookmark.setProduct_name(cursor.getString(cursor
						.getColumnIndexOrThrow(PRODUCT_NAME_COLUMN)));
				bookmarkList.add(bookmark);
			} while (cursor.moveToNext());
		}

		this.close();
		return bookmarkList;
	}

	public long insertBookmark(String product_id, String product_name) {
		this.open();
		long ret = -1;
		ContentValues value = new ContentValues();
		value.put(PRODUCT_ID_COLUMN, product_id);
		value.put(PRODUCT_NAME_COLUMN, product_name);

		ret = mDb.insert(TABLE_NAME, null, value);
		this.close();
		return ret;
	}

	public boolean exist(String product_id) {
		this.open();
		Cursor cursor = mDb.query(TABLE_NAME,
				new String[] { PRODUCT_ID_COLUMN }, PRODUCT_ID_COLUMN + " = "
						+ product_id, null, null, null, null);

		if (cursor != null && cursor.getCount() > 0) {
			this.close();
			return true;
		}
		this.close();
		return false;
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {
			super(context, "mcn", null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(TABLE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
			onCreate(db);
		}
	}
}
