package com.example.mcn.adpater;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mcn.R;
import com.example.mcn.model.Deal;

public class NearMeAdapter extends BaseAdapter {

	private static NearMeAdapter instance = null;

	Context context;
	private ListView listView;
	ArrayList<Deal> listDeal = new ArrayList<Deal>();
	View row;
	
	public static NearMeAdapter getInstance() {
		if (instance == null) {
			instance = new NearMeAdapter();
		}
		return instance;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public ArrayList<Deal> getListDeal() {
		return listDeal;
	}

	public void setListDeal(ArrayList<Deal> listDeal) {
		this.listDeal = listDeal;
	}

	public NearMeAdapter() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listDeal.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listDeal.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		
		row = inflater.inflate(R.layout.single_row, listView, false);
		TextView title = (TextView) row.findViewById(R.id.tvTitle);
		// TextView des = (TextView)row.findViewById(R.id.tvDes);
		ImageView image = (ImageView) row.findViewById(R.id.imVoucher);
		//TextView old_price = (TextView) row.findViewById(R.id.tvOldPrice);
		//TextView new_price = (TextView) row.findViewById(R.id.tvNewPrice);
		TextView percent = (TextView) row.findViewById(R.id.tvPercent);
		//TextView customer = (TextView) row.findViewById(R.id.tvCustomer);
		TextView address = (TextView) row.findViewById(R.id.tvAddress);
		//TextView dead_line = (TextView) row.findViewById(R.id.tvDeadLine);
		TextView distance = (TextView)row.findViewById(R.id.tvDistance);
		

		final Deal temp = listDeal.get(position);
		

		
		title.setText(temp.getName());
		distance.setVisibility(View.VISIBLE);
		if(temp.getDistance() > 1){
			distance.setText(temp.getDistance()+" km");
			}
			else
			{
				DecimalFormat newFormat = new DecimalFormat("#0");
				distance.setText(newFormat.format(temp.getDistance()*1000)+" m");
				
			}
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		symbols.setGroupingSeparator('.');
		DecimalFormat newFormat = new DecimalFormat("###,###", symbols);
		//old_price.setText(newFormat.format(temp.getPrice()) + "");
		//old_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
		
		//new_price.setText(newFormat.format(temp.getSell_price()) + "");
		address.setText(temp.getAddress());
		//dead_line.setText(temp.getDead_line());
		// des.setText(temp.description);
		//image.setImageDrawable(new BitmapDrawable(context
				//.getResources(), temp.getImg_bm()));

		
		percent.setText("-"+ temp.getPrice() + "%");
		//temp.setImageDeal(context, image);
		//image.setImageResource(R.drawable.deal_icon);
		
		return row;
	}

}
