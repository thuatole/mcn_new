package com.example.mcn.adpater;

import java.util.ArrayList;

import com.example.mcn.R;
import com.example.mcn.model.CommentSingle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ListCmtAdapter extends BaseAdapter {

	private static ListCmtAdapter instance = null;

	Context context;
	private ListView listView;
	ArrayList<CommentSingle> listCmt = new ArrayList<CommentSingle>();
	View row;
	
	public static ListCmtAdapter getInstance() {
		if (instance == null) {
			instance = new ListCmtAdapter();
		}
		return instance;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public ArrayList<CommentSingle> getListCmt() {
		return listCmt;
	}

	public void setListCmt(ArrayList<CommentSingle> listCmt) {
		this.listCmt = listCmt;
	}

	public ListCmtAdapter() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listCmt.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listCmt.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(context.LAYOUT_INFLATER_SERVICE);
		
		row = inflater.inflate(R.layout.single_row_comment_full, listView, false);
		TextView name = (TextView) row.findViewById(R.id.tvNameCmt);
		// TextView des = (TextView)row.findViewById(R.id.tvDes);
		ImageView image = (ImageView) row.findViewById(R.id.imPersion);
		TextView date = (TextView) row.findViewById(R.id.tvDate);
		//TextView customer = (TextView) row.findViewById(R.id.tvCustomer);
		TextView content = (TextView) row.findViewById(R.id.tvContentCmt);
		TextView score = (TextView) row.findViewById(R.id.tvScore);
		

		final CommentSingle temp = listCmt.get(position);
		
		name.setText(temp.getName());
		date.setText(temp.getDate());
		content.setText(temp.getContent());
		score.setText(temp.getScore());
		
		return row;
	}
}
