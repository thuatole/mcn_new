package com.example.mcn;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.mcn.AppSettings.API;
import com.example.utilities.AppSharedPreferenceUtil;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class UserDetail extends Activity {

	private TextView tvName, tvNickName, tvEmail, tvPhone, tvDOB, tvAddress;
	private ProgressDialog mDialog;
	private Button btnUpdate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_detail);
		
		tvName = (TextView)findViewById(R.id.tvName);
		tvNickName = (TextView)findViewById(R.id.tvNickname);
		tvEmail = (TextView)findViewById(R.id.tvEmail);
		tvPhone = (TextView)findViewById(R.id.tvPhone);
		tvDOB = (TextView)findViewById(R.id.tvDOB);
		tvAddress = (TextView)findViewById(R.id.tvAddress);
		btnUpdate = (Button)findViewById(R.id.btnEdit);
				
		// Show process dialog
		showProcessDialog();
		
		HttpRequestApi reAPI = new HttpRequestApi(getApplicationContext(),
				API.DATA_USER);
		AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
				this);
		reAPI.addParam("auth_key",appPreference
				.getString(AppSettings.PREFERENCE_AUTH_KEY));
		// Log.i("load API", list.size() + "ss1");
		reAPI.execute(new HttpRequestListener<JSONObject>() {
			// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
				
				
			}

			// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
			@Override
			public void onPostExecute(JSONObject result) {
				// TODO Auto-generated method stub
				try {
					// System.out.println("====");
					// System.out.println(result.get("dealDetail"));

					String name = result.getString("name");
					tvName.setText(name);
					
					String nickname = result.getString("name");
					tvNickName.setText(nickname);
					
					
					String phone = result.getString("phone");
					tvPhone.setText(phone);
					
					String email = result.getString("email");
					tvEmail.setText(email);
					
					String district = result.getString("district");
					tvDOB.setText(district);
					
					String address = result.getString("address");
					tvAddress.setText(address);
					
					// Hide process dialog 
					dismissProcessDialog();
				} catch (Exception e) {
					e.printStackTrace();
					// Hide process dialog
					dismissProcessDialog();
				}
			}
		}); // END reAPI.execute
		
		
		btnUpdate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent detail= new Intent(UserDetail.this, UserEdit.class);
				startActivity(detail);
			}
		});
	}

	private void showProcessDialog() {
		if (mDialog == null)
			mDialog = new ProgressDialog(this);
		mDialog.setMessage("Loading...");
		mDialog.setCancelable(false);
		if (!mDialog.isShowing()) {
			mDialog.show();
		}
	}

	private void dismissProcessDialog() {
		if (mDialog == null)
			mDialog = new ProgressDialog(this);
		if (mDialog.isShowing()) {
			mDialog.dismiss();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(AppSettings.CHECK_UPDATE_INFO == 1){
		finish();
		startActivity(getIntent());
		AppSettings.CHECK_UPDATE_INFO = 0;
		}
	}
	
	
	
}
