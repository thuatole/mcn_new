package com.example.mcn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.mcn.AppSettings.API;
import com.example.mcn.adpater.ScoreCollectAdapter;
import com.example.mcn.model.ScoreInfo;
import com.example.utilities.AppSharedPreferenceUtil;
import com.squareup.picasso.Picasso;

public class ScoreCollectActivity extends Activity {

	private ListView listViewSave, listViewUse;
	private ScoreCollectAdapter scoreSaveAdapter, scoreUseAdapter ;
	private ArrayList<ScoreInfo> listSave, listUse;
	private Context context;
	private Spinner choose_month;
	private  ProgressDialog progress;
	private TextView point_balance, sum_save_month;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.score_collect);
		
		listViewSave = (ListView)findViewById(R.id.list_save_month);
		listViewUse = (ListView)findViewById(R.id.list_used_month);
		choose_month = (Spinner)findViewById(R.id.list_month);
		point_balance = (TextView)findViewById(R.id.point_balance);
		sum_save_month = (TextView)findViewById(R.id.sum_save_month);
		
		listSave = new ArrayList<ScoreInfo>();
		listUse = new ArrayList<ScoreInfo>();
		context = ScoreCollectActivity.this;
		
		scoreSaveAdapter = ScoreCollectAdapter.getInstance();
		scoreSaveAdapter.setContext(context);

		scoreSaveAdapter.setListScore(listSave);

		listViewSave.setAdapter(scoreSaveAdapter);
		
		
		Calendar calendar = Calendar.getInstance();

		int thisYear = calendar.get(Calendar.YEAR);
		Log.i("year", thisYear+"");

		int thisMonth = calendar.get(Calendar.MONTH);
		thisMonth = thisMonth +1;//because January return 0 , not 1
		Log.i("month", thisMonth+"");
		
		//String[] spinnerDate = new String[12];
		List<String> spinnerDate =  new ArrayList<String>();
		if (thisMonth > 11 )
		{
			for (int i = 1; i <= 12; i++)
			{
				if(i < 10)
				{
					spinnerDate.add("0"+i+"/"+thisYear);
				}
				else
				{
					spinnerDate.add(i+"/"+thisYear);
				}
			}
		}
		
		
		else
		{
			for (int i = 1; i <= thisMonth; i++)
			{
				if(i < 10)
				{
					spinnerDate.add("0"+i+"/"+thisYear);
				}
				else
				{
					spinnerDate.add(i+"/"+thisYear);
				}
			}
			
			for (int i = 12; i > thisMonth; i--)
			{
				if(i < 10)
				{
					spinnerDate.add("0"+i+"/"+(thisYear-1));
				}
				else
				{
					spinnerDate.add(i+"/"+(thisYear-1));
				}
			}
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
			    this, android.R.layout.simple_spinner_item, spinnerDate);

			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		choose_month.setAdapter(adapter);
//		scoreUseAdapter = ScoreCollectAdapter.getInstance();
//		scoreUseAdapter.setContext(context);
//
//		scoreUseAdapter.setListScore(listUse);
//
//		listViewUse.setAdapter(scoreUseAdapter);
		
		
//		for (int i = 0; i < 5; i++) {
//				ScoreInfo score_use = new ScoreInfo("01/01/2015", "Lẩu dê 404", "135.000 VNĐ");
//				listSave.add(score_use);
//				scoreSaveAdapter.notifyDataSetChanged();
				
//				ScoreInfo score_save = new ScoreInfo("01/01/2035","Lẩu dê 808" , "351.000 VNĐ");
//				listUse.add(score_save);
//				scoreUseAdapter.notifyDataSetChanged();
//		}
		
		listViewSave.setOnTouchListener(new ListView.OnTouchListener() {
	        @Override
	        public boolean onTouch(View v, MotionEvent event) {
	            int action = event.getAction();
	            switch (action) {
	            case MotionEvent.ACTION_DOWN:
	                // Disallow ScrollView to intercept touch events.
	                v.getParent().requestDisallowInterceptTouchEvent(true);
	                break;

	            case MotionEvent.ACTION_UP:
	                // Allow ScrollView to intercept touch events.
	                v.getParent().requestDisallowInterceptTouchEvent(false);
	                break;
	            }

	            // Handle ListView touch events.
	            v.onTouchEvent(event);
	            return true;
	        }
	    });
		
		listViewUse.setOnTouchListener(new ListView.OnTouchListener() {
	        @Override
	        public boolean onTouch(View v, MotionEvent event) {
	            int action = event.getAction();
	            switch (action) {
	            case MotionEvent.ACTION_DOWN:
	                // Disallow ScrollView to intercept touch events.
	                v.getParent().requestDisallowInterceptTouchEvent(true);
	                break;

	            case MotionEvent.ACTION_UP:
	                // Allow ScrollView to intercept touch events.
	                v.getParent().requestDisallowInterceptTouchEvent(false);
	                break;
	            }

	            // Handle ListView touch events.
	            v.onTouchEvent(event);
	            return true;
	        }
	    });
		
		choose_month.setOnItemSelectedListener(new OnItemSelectedListener() {
			
			 public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
			        // your code here
				 Log.i("list", "change");
				 listSave.removeAll(listSave);
				 scoreSaveAdapter.notifyDataSetChanged();
				 String month = parentView.getItemAtPosition(position).toString();
				 
				 HttpRequestApi reAPI = new HttpRequestApi(getApplicationContext(),
							API.DATA_MONTH);
					AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
							ScoreCollectActivity.this);
					reAPI.addParam("auth_key",appPreference
							.getString(AppSettings.PREFERENCE_AUTH_KEY));
					reAPI.addParam("month", month);
					// Log.i("load API", list.size() + "ss1");
					reAPI.execute(new HttpRequestListener<JSONObject>() {
						// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							
							ScoreCollectActivity.this.progressDialog();
							
							
						}

						// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
						@Override
						public void onPostExecute(JSONObject result) {
							// TODO Auto-generated method stub
							try {
								// System.out.println("====");
								// System.out.println(result.get("dealDetail"));

								JSONArray jsonDeals = (JSONArray) result
										.get("histories");
								if(jsonDeals.length() >0)
								{
									listViewSave.setVisibility(View.VISIBLE);
									for (int i = 0; i < jsonDeals.length(); i++) {
										JSONObject jdeal = (JSONObject) jsonDeals
												.get(i);
										String time = jdeal.getString("time");
										String detail = jdeal.getString("detail");
										String money_save = jdeal.getInt("saved")+"";
										ScoreInfo score = new ScoreInfo(time, detail, money_save);
										listSave.add(score);
										scoreSaveAdapter.notifyDataSetChanged();
									}
								}
								else
								{
//									ScoreInfo score_use = new ScoreInfo("Không có thông tin", "Không có thông tin", "Không có thông tin");
//									listSave.add(score_use);
//									scoreSaveAdapter.notifyDataSetChanged();
									listViewSave.setVisibility(View.GONE);
								}
								
								String total = result.getInt("total")+"";
								String total_all = result.getInt("total_all_time")+"";
								
								sum_save_month.setText(total + " VNĐ");
								point_balance.setText(total_all + " VNĐ");
								
								progress.dismiss();
								// Log.i("load", jsonDeals + "");
							} catch (Exception e) {
								// TODO Auto-generated catch block
								//e.printStackTrace();
								
							}

							// Toast.makeText(getApplicationContext(), ""+result.toString(),
							// Toast.LENGTH_LONG).show();
						}
					}); // END reAPI.execute
			    }

			    public void onNothingSelected(AdapterView<?> parentView) {
			        // your code here
			    	Log.i("list", "unchange");
			    }
		});
		
	}

	protected void progressDialog()
	{
		ProgressBar pb = new ProgressBar(ScoreCollectActivity.this);
		
		pb.setIndeterminate(true);
		Drawable draw= getResources().getDrawable(R.drawable.customprogressbar);
		//pb.setProgressDrawable(draw);
		pb.setIndeterminateDrawable(draw);
		
		progress = new ProgressDialog(ScoreCollectActivity.this);
		
		progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		progress.show();
		progress.setContentView(pb);
	}
}
