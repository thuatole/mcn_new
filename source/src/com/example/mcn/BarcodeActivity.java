package com.example.mcn;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.mcn.AppSettings.API;
import com.example.utilities.AppSharedPreferenceUtil;
import com.google.android.gms.internal.co;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class BarcodeActivity extends Activity {
	private Button btn_scan;
	private Button btn_send;
	private EditText barCodeNumb;
	private EditText valMoney;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.barcode);
		
		btn_scan = (Button)findViewById(R.id.btnScan);
		btn_send = (Button)findViewById(R.id.btnAgree);
		barCodeNumb = (EditText)findViewById(R.id.etCode);
		valMoney = (EditText)findViewById(R.id.etMoney);
		
		// Action click
		btn_scan.setOnClickListener(clickScan);
		btn_send.setOnClickListener(clickSent);
	}
	
	// Action scan barcode
	private OnClickListener clickScan = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(getApplicationContext(), ScanCamera.class);
			startActivityForResult(intent, 0);
		}
	};
	
	// Action send 
	private OnClickListener clickSent = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if(barCodeNumb.getText().toString().equals("") || valMoney.getText().toString().equals("")) {
				alertWarning("Please scan barcode or input money");
			} else {
				try {
					int numValue = Integer.parseInt(valMoney.getText().toString());
					if(numValue <= 3000000) {
						String textValue = valMoney.getText().toString();
						AlertDialog.Builder builder = new AlertDialog.Builder(BarcodeActivity.this);
						builder.setMessage("Bạn vừa nhập " + textValue + " VNĐ")
						   .setCancelable(false)
						   .setPositiveButton("Có", new DialogInterface.OnClickListener() {
						       public void onClick(DialogInterface dialog, int id) {
						            requestServer();
						       }
						   })
						   .setNegativeButton("Không", new DialogInterface.OnClickListener() {
						       public void onClick(DialogInterface dialog, int id) {
						            dialog.cancel();
						       }
						   });
						AlertDialog alert = builder.create();
						alert.show();
					} else {
						alertWarning("Chỉ được nhập tối đa 3.000.000 VNĐ");
					}
				} catch (NumberFormatException e) {
					alertWarning("Vui lòng nhập đúng số tiền.");
				}
			}
		}
	};
	
	// Request
	private void requestServer() {
		AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(getApplicationContext());
		String authKey = appPreference.getString(AppSettings.PREFERENCE_AUTH_KEY);
		
		// Request to server
		HttpRequestApi request = new HttpRequestApi(getApplicationContext(), API.DATA_BARCODE);
		request.addParam("auth_key", authKey);
		request.addParam("store_code", barCodeNumb.getText().toString());
		request.addParam("amount", valMoney.getText().toString());
		
		request.execute(new HttpRequestListener<JSONObject>() {
			
			@Override
			public void onPreExecute() {
				
			}
			
			@Override
			public void onPostExecute(JSONObject result) {
				try {
					if(result != null) {
						int code = result.getInt("code");
						if(code == 0) {
							alertWarning("Success");
							barCodeNumb.setText("");
							valMoney.setText("");
						} else {
							if(code == -4) {
								alertWarning("Thẻ đã hết hạn, xin vui lòng gia hạn thêm.");
							} else {
								alertWarning("Vui lòng kiểm tra lại mã hoặc số tiền.");
							}
						}
					} else {
						alertWarning("Lỗi, xin vui lòng thử lại.");
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == 0) {
			if(resultCode == RESULT_OK) {
				String result=data.getStringExtra("varBarCode");
				barCodeNumb.setText(result);
			}
		}
	}
	
	private void alertWarning(String msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("MCN");
		builder.setMessage(msg);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
