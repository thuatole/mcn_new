package com.example.mcn;

import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class Map extends Activity implements RoutingListener {

	private Context context;
	private GoogleMap map;
	protected LatLng start;
	protected LatLng end;
	private Double lat, lng;
	private String  product_id, address, name;
	private Button btnFindWay;
	private int k = 0;// //check Ä‘á»ƒ khi tá»« setting resume láº¡i sáº½ reload láº¡i
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		
		btnFindWay = (Button)findViewById(R.id.btnFindWay);
		
		Bundle gotLat = getIntent().getExtras();
		lat = Double.parseDouble(gotLat.getString("lat"));
		Bundle gotLng = getIntent().getExtras();
		lng = Double.parseDouble(gotLng.getString("lng"));
		Bundle gotDealId = getIntent().getExtras();
		product_id = gotDealId.getString("product_id");
		Bundle gotName = getIntent().getExtras();
		name = gotName.getString("name");
		Bundle gotAdd = getIntent().getExtras();
		address = gotAdd.getString("address");
		
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
				.getMap();
		
		map.setMyLocationEnabled(true);

		map.animateCamera(CameraUpdateFactory.newLatLngZoom(
				new LatLng(lat, lng), 14.0f));

		map.addMarker(new MarkerOptions()
				.position(new LatLng(lat, lng))
				.title(name)
				.snippet(address)
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.icon_marker)));

//		map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
//
//			@Override
//			public void onInfoWindowClick(Marker marker1) {
//				// TODO Auto-generated methodif(marker1 == marker){
//				// Toast.makeText(getApplicationContext(), "Marker = " +
//				// marker1.getTitle(), Toast.LENGTH_SHORT).show();
//
//			}
//		});
		
		btnFindWay.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Location myLocation = map.getMyLocation();
				if (myLocation != null) {

					Double myLat = myLocation.getLatitude();
					Double myLng = myLocation.getLongitude();
					// Toast.makeText(getApplicationContext(), myLat + "+" +
					// myLng,
					// Toast.LENGTH_LONG).show();
					CameraUpdate center = CameraUpdateFactory
							.newLatLng(new LatLng(lat, lng));
					CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);

					map.moveCamera(center);
					map.animateCamera(zoom);

					start = new LatLng(lat, lng);
					end = new LatLng(myLat, myLng);

					Routing routing = new Routing(Routing.TravelMode.WALKING);
					routing.registerListener(Map.this);
					routing.execute(start, end);

				} else {
					// code yÃªu cáº§u báº­t gps
					// Intent callGPSSettingIntent = new
					// Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					// startActivityForResult(callGPSSettingIntent, 0);
					buildAlertMessageNoGps();

				}
			}
		});
	}

	@Override
	public void onRoutingFailure() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRoutingStart() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRoutingSuccess(PolylineOptions mPolyOptions) {
		// TODO Auto-generated method stub
		PolylineOptions polyoptions = new PolylineOptions();
		polyoptions.color(Color.RED);
		polyoptions.width(4);
		polyoptions.addAll(mPolyOptions.getPoints());
		map.addPolyline(polyoptions);

		// Start marker
		MarkerOptions options = new MarkerOptions();
		options.position(start);
		options.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.icon_marker));
		map.addMarker(options);

		// End marker
		options = new MarkerOptions();
		options.position(end);
		options.title("You here!");
		options.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.icon_marker_me));
		map.addMarker(options);
	}
	
	private void buildAlertMessageNoGps() {

		final Dialog dialog = new Dialog(Map.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_system);
		TextView tvTitle_dl = (TextView) dialog.findViewById(R.id.tvTitle);
		TextView tvContent_dl = (TextView) dialog.findViewById(R.id.tvContent);
		TextView btnDong = (TextView) dialog.findViewById(R.id.tvDong);
		TextView btnDongy = (TextView) dialog.findViewById(R.id.tvDongy);
		tvTitle_dl.setText("Thông báo");
		tvContent_dl
				.setText("Yêu cầu bật GPS!");
		dialog.show();
		btnDong.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				onBackPressed();
			}
		});
		btnDongy.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(
						android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
				k = 1;
			}
		});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		

		if (k == 1) {
			k = 0;
			finish();
			startActivity(getIntent());

		}

	}

}
