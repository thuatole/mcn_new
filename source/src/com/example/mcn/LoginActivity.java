package com.example.mcn;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.mcn.AppSettings.API;
import com.example.utilities.ConnectionDetector;
import com.example.utilities.AppSharedPreferenceUtil;
import com.example.utilities.PhoneUtil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class LoginActivity extends Activity {

	private boolean check_internet = true;
	private Button btnLogin;
	private EditText userName, passWord;
	private  ProgressDialog progress;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		btnLogin = (Button)findViewById(R.id.btnLogin);
		userName = (EditText)findViewById(R.id.etUserName);
		passWord = (EditText)findViewById(R.id.etPass);
		
		final boolean check_internet = LoginActivity.this.checkInternetStatus();
		
		if(check_internet == false)
		{
			Intent mIntent = new Intent(this, CheckConnection.class);
			mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			this.startActivity(mIntent);
		}
		
		SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
		boolean check_login = p.getBoolean("login_status", false);
		if(check_login == true){
		
			Intent main = new Intent(LoginActivity.this, MainActivity.class);
			startActivity(main);
			finish();
		}
		
		
		btnLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent main = new Intent(LoginActivity.this, MainActivity.class);
//				startActivity(main);
			
				if(userName.getText().length() == 0 || passWord.getText().length() == 0)
				{
					Toast.makeText(LoginActivity.this, "Vui lòng nhập đầy đủ user name và password !", Toast.LENGTH_LONG).show();
				}
				else{
				String deviceId = PhoneUtil.getDeviceId(getBaseContext(),
						getContentResolver());
				HttpRequestApi reAPI = new HttpRequestApi(
						getApplicationContext(), API.REG_DEVICE);
				reAPI.addParam("hid", deviceId);
				reAPI.addParam("type", "1");
				reAPI.addParam("username", userName.getText().toString());
				reAPI.addParam("password", passWord.getText().toString());
				// Log.i("load API", list.size() + "ss1");
				reAPI.execute(new HttpRequestListener<JSONObject>() {
					// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						LoginActivity.this.progressDialog();
					}

					// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
					@Override
					public void onPostExecute(JSONObject result) {
						// TODO Auto-generated method stub
						try {
//							JSONArray jsonDeals = (JSONArray) result
//									.get("deals");
							if(result.getInt("code") == 0){
							String authKey = result.getString("auth_key");
							AppSettings.DEVICE_AUTH_KEY = authKey;
							Log.i("auth", authKey);
							
							AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(LoginActivity.this);
							appPreference.putString(AppSettings.PREFERENCE_AUTH_KEY, authKey);
							SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
							Editor editor = sp.edit();
							editor.putBoolean("login_status", true);
							editor.commit();
							Intent main = new Intent(LoginActivity.this, MainActivity.class);
							startActivity(main);
							finish();
							}
							else
							{
								Toast.makeText(LoginActivity.this, "Sai mật khẩu hoặc password !", Toast.LENGTH_LONG).show();
							}
							progress.dismiss();
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						// Toast.makeText(getApplicationContext(),
						// ""+result.toString(),
						// Toast.LENGTH_LONG).show();
					}
				}); // END reAPI.execute
				}//end check user name + password
				}
				
		});//end btn login
		
	}

	
	protected void progressDialog()
	{
		ProgressBar pb = new ProgressBar(LoginActivity.this);
		
		pb.setIndeterminate(true);
		Drawable draw= getResources().getDrawable(R.drawable.customprogressbar);
		//pb.setProgressDrawable(draw);
		pb.setIndeterminateDrawable(draw);
		
		progress = new ProgressDialog(LoginActivity.this);
		
		progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		progress.show();
		progress.setContentView(pb);
	}

	public boolean checkInternetStatus() { // TODO Auto-generated method stub
		boolean a = true;
		ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
		boolean isConnecting = cd.isConnectingToInternet();
		if (!isConnecting) {
			// showAlertDialog(Splash.this.getApplicationContext(),
			// "No Internet Connection",
			// "You don't have internet connection.", false);
			a = false;
		}
		return a;
	}
}
