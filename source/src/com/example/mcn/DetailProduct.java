package com.example.mcn;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.mcn.AppSettings.API;
import com.example.mcn.R.id;
import com.example.mcn.adpater.BookmarkDBAdapter;
import com.example.mcn.adpater.CommentInDetailScreenAdapter;
import com.example.mcn.adpater.NearMeAdapter;
import com.example.mcn.model.CommentSingle;
import com.example.mcn.model.Deal;
import com.example.utilities.AppSharedPreferenceUtil;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailProduct extends Activity implements OnItemClickListener {

	private Context context;
	private String cateId, product_id, address, name, lat, lng;
	private TextView tvName, tvAddress, tvToMap, tvIdPretty, tvPercent, tvPhone, btnShare, btnSave, btnBackTop, btnGoToComment, tvExp;
	private Button btnLoadMoreCmt;
	private WebView   wvContent;
	private ImageView imageProduct;
	private  ProgressDialog progress;
	private ScrollView scrDetail;
	private String mTextShare;
	private String url_share;
	private ListView listView;
	private CommentInDetailScreenAdapter cmtAdapter;
	private ArrayList<CommentSingle> list;
	private int total_products;
	
	@SuppressLint("SetJavaScriptEnabled") @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail_product);
		
		scrDetail = (ScrollView)findViewById(R.id.scrDetail);
		tvName = (TextView)findViewById(R.id.tvName);
		tvAddress = (TextView)findViewById(R.id.tvAddress);
		tvToMap = (TextView)findViewById(R.id.tvSeeMap);
		tvIdPretty = (TextView)findViewById(R.id.tvIdPretty);
		tvPercent = (TextView)findViewById(R.id.tvPercent);
		tvPhone = (TextView)findViewById(R.id.tvPhone);
		tvExp = (TextView)findViewById(R.id.tvDeadline);
		btnShare = (TextView)findViewById(R.id.btnShare);
		btnSave = (TextView)findViewById(R.id.btnSave);
		btnBackTop = (TextView)findViewById(R.id.btnBackToTop);
		wvContent = (WebView)findViewById(R.id.wvDetail);
		imageProduct = (ImageView)findViewById(R.id.imgThumb);
		btnGoToComment = (TextView)findViewById(R.id.btnCmt);
		btnLoadMoreCmt = (Button)findViewById(R.id.btnLoadMoreComment);
		
		listView = (ListView) findViewById(R.id.listCmt);
		listView.setOnItemClickListener(this);
		
		
		
		
		Bundle gotId = getIntent().getExtras();
		product_id = gotId.getString("product_id");
		context = DetailProduct.this;
		list = new ArrayList<CommentSingle>();

		
		HttpRequestApi reAPI = new HttpRequestApi(getApplicationContext(),
				API.DATA_DETAIL);
		AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
				this);
		reAPI.addParam("auth_key",appPreference
				.getString(AppSettings.PREFERENCE_AUTH_KEY));
		reAPI.addParam("id", product_id);
		// Log.i("load API", list.size() + "ss1");
		reAPI.execute(new HttpRequestListener<JSONObject>() {
			// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub
				
				DetailProduct.this.progressDialog();
				
				
			}

			// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
			@SuppressLint("SetJavaScriptEnabled") @Override
			public void onPostExecute(JSONObject result) {
				// TODO Auto-generated method stub
				try {
					// System.out.println("====");
					// System.out.println(result.get("dealDetail"));

					JSONObject jsonDeals = (JSONObject) result
							.get("product");
					name = jsonDeals.getString("name").toString();
					url_share = jsonDeals.getString("share_url").toString();
					tvName.setText(name);
					String content = jsonDeals.getString("body");
					
					if(content == "null"){
						wvContent.getSettings().setJavaScriptEnabled(true);
						wvContent.loadDataWithBaseURL("", "<p>ChÆ°a cÃ³ mÃ´ táº£ cho deal nÃ y</p>", "text/html", "UTF-8", "");
					}
					else{
						wvContent.getSettings().setJavaScriptEnabled(true);
						wvContent.loadDataWithBaseURL("", content, "text/html", "UTF-8", "");
					}
					
					lat = jsonDeals.getString("lat");
					lng = jsonDeals.getString("lng");
					address = jsonDeals.getString("address");
					tvAddress.setText(address);
					JSONArray jsonImages = (JSONArray) jsonDeals
							.get("images");
					Log.i("images", jsonImages + "");
					Picasso.with(context).load(jsonImages.getString(0)).resize(720, 1280)
					  .into(imageProduct); //url is image url
					tvExp.setText("Ngày hết hạn: " + jsonDeals.getString("expire_day"));
					tvIdPretty.setText("ID:"+jsonDeals.getString("id_pretty"));
					tvPercent.setText("-"+jsonDeals.getString("discount")+"%");
					tvPhone.setText(jsonDeals.getString("phone"));
					progress.dismiss();
					
					// Get text share
					mTextShare = name + "." + address + "\n" + url_share;
					// Log.i("load", jsonDeals + "");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					
				}
			}
		}); // END reAPI.execute
		
		
		tvToMap.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bundle lat_bundle = new Bundle();
				lat_bundle.putString("lat", lat);
				Bundle lng_bundle = new Bundle();
				lng_bundle.putString("lng", lng);
				Bundle deal_id_bundle = new Bundle();
				deal_id_bundle.putString("product_id", product_id);
				Bundle name_bundle = new Bundle();
				name_bundle.putString("name", name);
				Bundle address_bundle = new Bundle();
				address_bundle.putString("address", address);
				Intent a = new Intent(DetailProduct.this, Map.class);
				a.putExtras(lat_bundle);
				a.putExtras(lng_bundle);
				a.putExtras(deal_id_bundle);
				a.putExtras(name_bundle);
				a.putExtras(address_bundle);
				startActivity(a);
			}
		});
		
		// Back action click
		btnBackTop.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//scrDetail.fullScroll(ScrollView.FOCUS_UP);
				Bundle lat_bundle = new Bundle();
				lat_bundle.putString("lat", lat);
				Bundle lng_bundle = new Bundle();
				lng_bundle.putString("lng", lng);
				Bundle deal_id_bundle = new Bundle();
				deal_id_bundle.putString("product_id", product_id);
				Bundle name_bundle = new Bundle();
				name_bundle.putString("name", name);
				Bundle address_bundle = new Bundle();
				address_bundle.putString("address", address);
				Intent a = new Intent(DetailProduct.this, Map.class);
				a.putExtras(lat_bundle);
				a.putExtras(lng_bundle);
				a.putExtras(deal_id_bundle);
				a.putExtras(name_bundle);
				a.putExtras(address_bundle);
				startActivity(a);
			}
		});
		
		// Share action click
		btnShare.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 Intent shareIntent = new Intent(Intent.ACTION_SEND);
                 shareIntent.setType("text/plain");
                 shareIntent.putExtra(Intent.EXTRA_TEXT, mTextShare);
                 startActivity(Intent.createChooser(shareIntent, mTextShare)); 
			}
		});
		
		// Bookmark action click
		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				BookmarkDBAdapter bookmarkDB = new BookmarkDBAdapter(
						DetailProduct.this);
				if (!bookmarkDB.exist(product_id)) {
					if(name.trim().equals("")) {
						name = tvName.getText().toString().trim();
					}
					if(!product_id.equals("") && !name.equals("")) {
						long ret = bookmarkDB.insertBookmark(product_id, name);
						if(ret > 0) {
							Toast.makeText(DetailProduct.this, "Lưu thành công", Toast.LENGTH_SHORT).show();
						}
					}
				} else {
					new AlertDialog.Builder(DetailProduct.this)
							.setTitle("Thông báo")
							.setMessage("Sản phẩm đã được lưu")
							.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											dialog.cancel();
										}
									}).show();
				}
			}
		});
		
		
		btnGoToComment.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bundle deal_id_bundle = new Bundle();
				deal_id_bundle.putString("product_id", product_id);
				Bundle name_bundle = new Bundle();
				name_bundle.putString("name", name);
				Bundle address_bundle = new Bundle();
				address_bundle.putString("address", address);
				Intent a = new Intent(DetailProduct.this, WriteComment.class);
				a.putExtras(deal_id_bundle);
				a.putExtras(name_bundle);
				a.putExtras(address_bundle);
				startActivity(a);
				
			}
		});
		
		btnLoadMoreCmt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bundle deal_id_bundle = new Bundle();
				deal_id_bundle.putString("product_id", product_id);
				Bundle name_bundle = new Bundle();
				name_bundle.putString("name", name);
				Bundle address_bundle = new Bundle();
				address_bundle.putString("address", address);
				Intent a = new Intent(DetailProduct.this, listComment.class);
				a.putExtras(deal_id_bundle);
				a.putExtras(name_bundle);
				a.putExtras(address_bundle);
				startActivity(a);
				
			}
		});
		HttpRequestApi reAPI_cmt = new HttpRequestApi(
				getApplicationContext(), API.GET_COMMENT);
		reAPI_cmt.addParam("product_id", product_id);
		reAPI_cmt.addParam("auth_key",appPreference
				.getString(AppSettings.PREFERENCE_AUTH_KEY));
		String auth  = appPreference
				.getString(AppSettings.PREFERENCE_AUTH_KEY);
		
		reAPI_cmt.execute(new HttpRequestListener<JSONObject>() {

			// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub

			}

			// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
			@Override
			public void onPostExecute(JSONObject result) {
				// TODO Auto-generated method stub

				try {
					if (result.getInt("code") == 0) {

						
						try {
							JSONArray jsonDeals = (JSONArray) result
									.get("comments");
							if(jsonDeals.length() > 0)
							{
								if(jsonDeals.length() > 3)
								{
									for (int i = 0; i < 3; i++) {

										JSONObject jdeal = (JSONObject) jsonDeals
												.get(i);
										String cmt_id = jdeal.get("comment_id").toString();
										// Log.i("load Begin", id);
										String name = jdeal.get("name").toString();

										String date = jdeal.get("time").toString();
										
										String content = jdeal.get("body").toString();
										
										CommentSingle cmt = new CommentSingle(name, date, "3.0", content);
										list.add(cmt);
									}
								}
								else
								{
									for (int i = 0; i < jsonDeals.length(); i++) {

										JSONObject jdeal = (JSONObject) jsonDeals
												.get(i);
										String cmt_id = jdeal.get("comment_id").toString();
										// Log.i("load Begin", id);
										String name = jdeal.get("name").toString();

										String date = jdeal.get("time").toString();
										
										String content = jdeal.get("body").toString();
										
										CommentSingle cmt = new CommentSingle(name, date, "3.0", content);
										list.add(cmt);
									}
								}
								
								cmtAdapter.notifyDataSetChanged();
							}
							else
							{
								
								listView.setVisibility(View.GONE);
							}
							
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}// end if code ==1
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});// end api
		
		cmtAdapter = CommentInDetailScreenAdapter.getInstance();
		cmtAdapter.setContext(context);

		cmtAdapter.setListCmt(list);

		listView.setAdapter(cmtAdapter);
		
//		for (int i = 0; i<3; i++)
//		{
//			
//			CommentSingle cmt = new CommentSingle("Thuat Ole", "03/02/2015", "3.0", "Quán ở đây quá ngon, tuy nhiên phục vụ hơi hcậm, nhưng nhân viên vui vẻ nhiệt tình với khách hàng");
//			list.add(cmt);
//			cmtAdapter.notifyDataSetChanged();
//		}
		
		
		
		
	}
	
	protected void progressDialog()
	{
		ProgressBar pb = new ProgressBar(DetailProduct.this);
		
		pb.setIndeterminate(true);
		Drawable draw= getResources().getDrawable(R.drawable.customprogressbar);
		//pb.setProgressDrawable(draw);
		pb.setIndeterminateDrawable(draw);
		
		progress = new ProgressDialog(DetailProduct.this);
		
		progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		progress.show();
		progress.setContentView(pb);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		
	}

	
}
