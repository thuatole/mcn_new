package com.example.mcn;

import java.text.NumberFormat;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

import com.example.mcn.AppSettings.API;
import com.example.mcn.adpater.NearMeAdapter;
import com.example.mcn.model.Deal;
import com.example.utilities.AppSharedPreferenceUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class NearMeActivity extends Activity implements LocationListener,
	OnItemClickListener {

	private GoogleMap map;
	private Context context;
	protected LatLng start;
	protected LatLng end;
	private ListView listView;
	private Double myLat, myLng, lat, lng;
	private int i = 0;// check Ä‘á»ƒ chá»‰ load vá»‹ trÃ­ láº§n Ä‘áº§u (vÃ¬ thuá»™c tÃ­nh
	// onLocationChanged load liÃªn tá»¥c)
	private int j = 0;// check Ä‘á»ƒ chá»‰ load adapter láº§n Ä‘áº§u (vÃ¬ thuá»™c tÃ­nh
	// onLocationChanged load liÃªn tá»¥c)
	
	private int k = 0;// //check Ä‘á»ƒ khi tá»« setting resume láº¡i sáº½ reload láº¡i
	// activity
	private NearMeAdapter nearDealAdapter;
	private ArrayList<Deal> list;
	private ProgressBar pb;
	private String name, address;
	private Button btnLoadMore;
	private int item_per_page = 10;
	private int offset = 0;
	private int total_products;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.near_me_activity);
		
		listView = (ListView) findViewById(R.id.listVoucher);
		
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
				.getMap();
		map.setMyLocationEnabled(true);

		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		//if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
		 if(!locationManager.isProviderEnabled(
		 LocationManager.NETWORK_PROVIDER) || !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) )
		{
			buildAlertMessageNoGps();
		} else {
			 locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
			 0, 0, this); //cháº¡y trÃªn mÃ¡y tháº­t
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 0, 0, this);// cháº¡y trÃªn mÃ¡y
																// áº£o
		}
		 
		 list = new ArrayList<Deal>();
		 context = NearMeActivity.this;
		 listView.setOnItemClickListener(this);
		 
		 btnLoadMore = new Button(this);
		 btnLoadMore.setText("Hiển thị thêm");
		 
		 
		 btnLoadMore.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// list.clear();
					
					//Log.i("list_test", offset+""
							//);
					if( (offset*item_per_page) < total_products ) 
					{
						HttpRequestApi reAPI = new HttpRequestApi(
								getApplicationContext(), API.DATA_NEAR);
						reAPI.addParam("quantity", item_per_page + "");
						reAPI.addParam("lat", myLat + "");
						reAPI.addParam("lng", myLng + "");
						reAPI.addParam("radius", "2000");
						reAPI.addParam("offset",offset + "");
						AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
								NearMeActivity.this);
						reAPI.addParam("auth_key",appPreference
								.getString(AppSettings.PREFERENCE_AUTH_KEY));
						
						reAPI.execute(new HttpRequestListener<JSONObject>() {

							// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
							@Override
							public void onPreExecute() {
								// TODO Auto-generated method stub

							}

							// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
							@Override
							public void onPostExecute(JSONObject result) {
								// TODO Auto-generated method stub

								try {
									if (result.getInt("total_products") > 0) {

										total_products = result.getInt("total_products");
										
										try {
											JSONArray jsonDeals = (JSONArray) result
													.get("products");
											
											for (int i = 0; i < jsonDeals.length(); i++) {

												JSONObject jdeal = (JSONObject) jsonDeals
														.get(i);
												String id = jdeal.get("id").toString();
												// Log.i("load Begin", id);
												name = jdeal.get("name").toString();
												String description = jdeal.get(
														"short_description").toString();
//												int price = jdeal.getInt("price");
//												int sell_price = jdeal
//														.getInt("sell_price");
//												NumberFormat defaultFormat = NumberFormat.getPercentInstance();
//												defaultFormat.setMinimumFractionDigits(0);
//												double sell_price_ = sell_price;
//												double price_ = price;
//												String price_percent = defaultFormat.format(sell_price_ / price_);
//												int sell_count = (Integer) jdeal
//														.get("sell_count");
		//
//												String category_id = jdeal.get(
//														"category_id").toString();
//												String provider = jdeal.get("provider")
//														.toString();
//												String content = jdeal.get("content")
//														.toString();
//												String dead_line = jdeal.get(
//														"dead_line").toString();
												address = jdeal.getString("address")
														.toString();
												lat = Double.parseDouble(jdeal.get(
														"lat").toString());
												lng = Double.parseDouble(jdeal.get(
														"lng").toString());
												Double distance = Double
														.parseDouble(jdeal.get(
																"distance").toString());
												int is_save = 1;
												int is_load = 1;
												int show_hide = 1;
												String img = "http://google.com";
												int percent = jdeal.getInt("discount");
//												if(jdeal.get("img").toString().matches("null"))
//												{
//													img = "http://google.com";
//													//Log.i("tv", jdeal.get("img").toString());
//												}
//												else{
//												img = jdeal.getJSONObject("img").get("external_url").toString();
//												}
//												
												 
												// Log.i("load_cate", jdeal.toString());
												Deal s = new Deal(id, name,
														description, percent, 123,
														123, "123",
														"123", "123", img,
														is_save, is_load, show_hide,
														"123", distance, lat, lng,
														address);
												
												list.add(s);
												
												nearDealAdapter.notifyDataSetChanged();
												Marker marker = map.addMarker(new MarkerOptions()
												.position(
														new LatLng(list.get(i).getLat(), list
																.get(i).getLng()))
												.icon(BitmapDescriptorFactory
														.fromResource(R.drawable.icon_marker))
												.title(list.get(i).getName())
												.snippet(list.get(i).getAddress()));
										NearMeActivity.this.animation_marker(marker);
										offset++;	

											}
											
											

											// Log.i("load End", "position: " + i +
											// "- id" +
											// s.getId());
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									}// end if code ==1
									else {
										final Dialog dialog = new Dialog(NearMeActivity.this);
										dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
										dialog.setContentView(R.layout.dialog_bad_deal_result);
										TextView tvTitle_dl = (TextView) dialog
												.findViewById(R.id.tvTitle);
										TextView tvContent_dl = (TextView) dialog
												.findViewById(R.id.tvContent);
										TextView btnDong = (TextView) dialog
												.findViewById(R.id.tvDong);
										tvTitle_dl.setText("Thông báo!");
										tvContent_dl
												.setText("Không còn sản phẩm gần bạn!");
										dialog.show();
										btnDong.setOnClickListener(new View.OnClickListener() {

											@Override
											public void onClick(View v) {
												// TODO Auto-generated method
												// stub
												dialog.dismiss();
												onBackPressed();
											}
										});
										listView.removeFooterView(pb);
									}// end else
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

						});// end api
						
						
						
					}
					
					
					else
					{
						final Dialog dialog = new Dialog(NearMeActivity.this);
						dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						dialog.setContentView(R.layout.dialog_bad_deal_result);
						TextView tvTitle_dl = (TextView) dialog
								.findViewById(R.id.tvTitle);
						TextView tvContent_dl = (TextView) dialog
								.findViewById(R.id.tvContent);
						TextView btnDong = (TextView) dialog
								.findViewById(R.id.tvDong);
						tvTitle_dl.setText("Thông báo !");
						tvContent_dl
								.setText("Đã tải hết dữ liệu");
						dialog.show();
						btnDong.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method
								// stub
								dialog.dismiss();
								//onBackPressed();
							}
						});
						listView.removeFooterView(btnLoadMore);
					}
					
				}
			});

	}

	

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
		if (location != null) {
			if (i == 0) {
				myLat = location.getLatitude();

				myLng = location.getLongitude();

				map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
						myLat, myLng), 13.0f));

				map.addMarker(new MarkerOptions()
				.position(new LatLng(myLat, myLng))
				.title("you here!"));
				//.icon(BitmapDescriptorFactory
				//.fromResource(R.drawable.icon_marker_me)));
				i = 1;

			}
			if (j == 0) {
				pb = new ProgressBar(getApplicationContext());
				pb.setIndeterminate(true);
				
				listView.addFooterView(pb);
				HttpRequestApi reAPI = new HttpRequestApi(
						getApplicationContext(), API.DATA_NEAR);
				reAPI.addParam("quantity", item_per_page + "");
				reAPI.addParam("lat", myLat + "");
				reAPI.addParam("lng", myLng + "");
				reAPI.addParam("radius", "2000");
				reAPI.addParam("offset",offset + "");
				AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
						this);
				reAPI.addParam("auth_key",appPreference
						.getString(AppSettings.PREFERENCE_AUTH_KEY));
				
				reAPI.execute(new HttpRequestListener<JSONObject>() {

					// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub

					}

					// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
					@Override
					public void onPostExecute(JSONObject result) {
						// TODO Auto-generated method stub

						try {
							if (result.getInt("total_products") > 0) {

								total_products = result.getInt("total_products");
								
								try {
									JSONArray jsonDeals = (JSONArray) result
											.get("products");
									
									for (int i = 0; i < jsonDeals.length(); i++) {

										JSONObject jdeal = (JSONObject) jsonDeals
												.get(i);
										String id = jdeal.get("id").toString();
										// Log.i("load Begin", id);
										name = jdeal.get("name").toString();
										String description = jdeal.get(
												"short_description").toString();
//										int price = jdeal.getInt("price");
//										int sell_price = jdeal
//												.getInt("sell_price");
//										NumberFormat defaultFormat = NumberFormat.getPercentInstance();
//										defaultFormat.setMinimumFractionDigits(0);
//										double sell_price_ = sell_price;
//										double price_ = price;
//										String price_percent = defaultFormat.format(sell_price_ / price_);
//										int sell_count = (Integer) jdeal
//												.get("sell_count");
//
//										String category_id = jdeal.get(
//												"category_id").toString();
//										String provider = jdeal.get("provider")
//												.toString();
//										String content = jdeal.get("content")
//												.toString();
//										String dead_line = jdeal.get(
//												"dead_line").toString();
										address = jdeal.getString("address")
												.toString();
										lat = Double.parseDouble(jdeal.get(
												"lat").toString());
										lng = Double.parseDouble(jdeal.get(
												"lng").toString());
										Double distance = Double
												.parseDouble(jdeal.get(
														"distance").toString());
										int is_save = 1;
										int is_load = 1;
										int show_hide = 1;
										String img = "http://google.com";
										int percent = jdeal.getInt("discount");
//										if(jdeal.get("img").toString().matches("null"))
//										{
//											img = "http://google.com";
//											//Log.i("tv", jdeal.get("img").toString());
//										}
//										else{
//										img = jdeal.getJSONObject("img").get("external_url").toString();
//										}
//										
										 
										// Log.i("load_cate", jdeal.toString());
										Deal s = new Deal(id, name,
												description, percent, 123,
												123, "123",
												"123", "123", img,
												is_save, is_load, show_hide,
												"123", distance, lat, lng,
												address);
										
										list.add(s);
										
										
										
										nearDealAdapter.notifyDataSetChanged();
										offset++;
										// Log.i("list_test",base_list.toString());
										// list.add(s);
										// nearDealAdapter.notifyDataSetChanged();
										
										map.addMarker(new MarkerOptions()
										.position(
												new LatLng(lat, lng))
										.icon(BitmapDescriptorFactory
												.fromResource(R.drawable.icon_marker))
										.title(name)
										.snippet(
												address));

									}
									
									listView.removeFooterView(pb);
									listView.addFooterView(btnLoadMore);

									// Log.i("load End", "position: " + i +
									// "- id" +
									// s.getId());
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}// end if code ==1
							else {
								final Dialog dialog = new Dialog(NearMeActivity.this);
								dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
								dialog.setContentView(R.layout.dialog_bad_deal_result);
								TextView tvTitle_dl = (TextView) dialog
										.findViewById(R.id.tvTitle);
								TextView tvContent_dl = (TextView) dialog
										.findViewById(R.id.tvContent);
								TextView btnDong = (TextView) dialog
										.findViewById(R.id.tvDong);
								tvTitle_dl.setText("Thông báo!");
								tvContent_dl
										.setText("Không còn sản phẩm gần bạn!");
								dialog.show();
								btnDong.setOnClickListener(new View.OnClickListener() {

									@Override
									public void onClick(View v) {
										// TODO Auto-generated method
										// stub
										dialog.dismiss();
										onBackPressed();
									}
								});
								listView.removeFooterView(pb);
							}// end else
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				});// end api

				nearDealAdapter = NearMeAdapter.getInstance();
				nearDealAdapter.setContext(context);

				nearDealAdapter.setListDeal(list);

				listView.setAdapter(nearDealAdapter);
				j = 1;

			}

		}//end location != 0

		else {
			Intent callGPSSettingIntent = new Intent(
					android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivityForResult(callGPSSettingIntent, 0);
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Bundle bundle = new Bundle();
		Deal clicked_deal = list.get(position);

		String bread = "" + clicked_deal.getId();
		bundle.putString("product_id", bread);
		Intent a = new Intent(NearMeActivity.this, DetailProduct.class);
		a.putExtras(bundle);
		startActivity(a);
	}

	protected void animation_marker(final Marker marker) {
		// TODO Auto-generated method stub
		final Handler handler = new Handler();
		final long start = SystemClock.uptimeMillis();
		final long duration = 1000;

		final Interpolator interpolator = new BounceInterpolator();

		handler.post(new Runnable() {
			@Override
			public void run() {
				long elapsed = SystemClock.uptimeMillis() - start;
				float t = Math.max(
						1 - interpolator.getInterpolation((float) elapsed
								/ duration), 0);
				marker.setAnchor(0.5f, 1.0f + 14 * t);

				if (t > 0.0) {
					// Post again 15ms later.
					handler.postDelayed(this, 15);
				} else {
					marker.showInfoWindow();

				}
			}
		});

	}
	private void buildAlertMessageNoGps() {
		
		 final Dialog dialog = new Dialog(
					NearMeActivity.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.dialog_system);
			TextView tvTitle_dl = (TextView) dialog
					.findViewById(R.id.tvTitle);
			TextView tvContent_dl = (TextView) dialog
					.findViewById(R.id.tvContent);
			TextView btnDong = (TextView) dialog
					.findViewById(R.id.tvDong);
			TextView btnDongy = (TextView) dialog
					.findViewById(R.id.tvDongy);
			tvTitle_dl.setText("Thông báo !");
			tvContent_dl.setText("Vui lòng bật GPS để lấy vị trí hiện tại của bạn?");
			dialog.show();
			btnDong.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					onBackPressed();
				}
			});
			btnDongy.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					 startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	                   k =1;
				}
			});
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		

		if (k == 1) {
			k = 0;
			finish();
			startActivity(getIntent());

		}

	}
	
}
