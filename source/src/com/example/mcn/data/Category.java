package com.example.mcn.data;

public class Category {
	private String title;
	private int image;
	private String description;
	private String country;
	private String city;
	private String link;

	public Category(String title, int image, String description,
			String country, String city, String link) {
		this.title = title;
		this.image = image;
		this.description = description;
		this.country = country;
		this.city = city;
		this.link = link;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getImage() {
		return image;
	}

	public void setImage(int image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
