package com.example.mcn;

import java.util.ArrayList;














import org.json.JSONArray;
import org.json.JSONObject;

import com.example.mcn.AppSettings.API;
import com.example.mcn.adpater.CommentInDetailScreenAdapter;
import com.example.mcn.adpater.ListCmtAdapter;
import com.example.mcn.model.CommentSingle;
import com.example.utilities.AppSharedPreferenceUtil;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class listComment extends Activity implements OnItemClickListener {

	private ActionBar actionBar;
	private Context context;
	private ListView listView;
	private Button btnCmt;
	private ListCmtAdapter cmtAdapter;
	private ArrayList<CommentSingle> list;
	private String  product_id, address, name;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_comment);
		btnCmt = (Button)findViewById(R.id.btnCmt);
		
		Bundle gotDealId = getIntent().getExtras();
		product_id = gotDealId.getString("product_id");
		Bundle gotName = getIntent().getExtras();
		name = gotName.getString("name");
		Bundle gotAdd = getIntent().getExtras();
		address = gotAdd.getString("address");
		
		context = listComment.this;
		list = new ArrayList<CommentSingle>();
		listView = (ListView) findViewById(R.id.listCmt);
		listView.setOnItemClickListener(this);
		
		
		actionBar = getActionBar();
		// actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setIcon(R.drawable.icon);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setCustomView(R.layout.action_bar_title_comment);
		
		 View v = actionBar.getCustomView();
		    TextView tvName = (TextView) v.findViewById(R.id.action_bar_title);
		    tvName.setText(name);
		    TextView tvAdd = (TextView) v.findViewById(R.id.action_bar_subtitle);
		    tvAdd.setText(address);
		
		btnCmt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bundle deal_id_bundle = new Bundle();
				deal_id_bundle.putString("product_id", product_id);
				Bundle name_bundle = new Bundle();
				name_bundle.putString("name", name);
				Bundle address_bundle = new Bundle();
				address_bundle.putString("address", address);
				Intent a = new Intent(listComment.this, WriteComment.class);
				a.putExtras(deal_id_bundle);
				a.putExtras(name_bundle);
				a.putExtras(address_bundle);
				startActivity(a);
				
			}
		});
		
		AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
				this);
		
		HttpRequestApi reAPI_cmt = new HttpRequestApi(
				getApplicationContext(), API.GET_COMMENT);
		reAPI_cmt.addParam("product_id", product_id);
		reAPI_cmt.addParam("auth_key",appPreference
				.getString(AppSettings.PREFERENCE_AUTH_KEY));
		String auth  = appPreference
				.getString(AppSettings.PREFERENCE_AUTH_KEY);
		
		reAPI_cmt.execute(new HttpRequestListener<JSONObject>() {

			// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
			@Override
			public void onPreExecute() {
				// TODO Auto-generated method stub

			}

			// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
			@Override
			public void onPostExecute(JSONObject result) {
				// TODO Auto-generated method stub

				try {
					if (result.getInt("code") == 0) {

						
						try {
							JSONArray jsonDeals = (JSONArray) result
									.get("comments");
							if(jsonDeals.length() > 0)
							{
								
								
									for (int i = 0; i < jsonDeals.length(); i++) {

										JSONObject jdeal = (JSONObject) jsonDeals
												.get(i);
										String cmt_id = jdeal.get("comment_id").toString();
										// Log.i("load Begin", id);
										String name = jdeal.get("name").toString();

										String date = jdeal.get("time").toString();
										
										String content = jdeal.get("body").toString();
										
										String score = jdeal.get("mark").toString();
										
										CommentSingle cmt = new CommentSingle(name, date, score, content);
										list.add(cmt);
									}
								
								
								cmtAdapter.notifyDataSetChanged();
							}
							else
							{
								
								listView.setVisibility(View.GONE);
							}
							
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}// end if code ==1
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});// end api
		
		cmtAdapter = ListCmtAdapter.getInstance();
		cmtAdapter.setContext(context);

		cmtAdapter.setListCmt(list);

		listView.setAdapter(cmtAdapter);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();

			return true;
		

		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
