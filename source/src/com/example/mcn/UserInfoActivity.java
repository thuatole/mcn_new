package com.example.mcn;

import java.util.ArrayList;

import javax.xml.datatype.Duration;

import com.example.mcn.MainActivity.adapter;
import com.example.mcn.MainActivity.singleRow;
import com.example.utilities.AppSharedPreferenceUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class UserInfoActivity extends Activity implements OnItemClickListener{

	private ListView listMenu;	
	private TextView userName;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_info);
		

		//adapter sliding menu
		listMenu = (ListView) findViewById(R.id.listMenu);
		listMenu.setAdapter(new adapter(this));
		listMenu.setOnItemClickListener(this);
		
		userName = (TextView)findViewById(R.id.userName);
		
		AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
				this);
		userName.setText(appPreference
				.getString("user_name"));
	}
	

	
	// list menu
			class singleRow {
				String menu_name;
				int image;

				public singleRow(String menu_name, int image) {
					// TODO Auto-generated constructor stub
					this.menu_name = menu_name;
					this.image = image;

				}

			}

			class adapter extends BaseAdapter {

				ArrayList<singleRow> list;
				Context context;

				public adapter(Context c) {
					context = c;
					// TODO Auto-generated constructor stub
					list = new ArrayList<singleRow>();

					Resources res = c.getResources();
					String[] menu_name = res.getStringArray(R.array.user_info_list);

					int[] images = { R.drawable.icon_thongtincanhan, R.drawable.icon_thongtinthe,
							R.drawable.icon_thongtinthe, R.drawable.icon_lichsumuahang
							 };
					for (int i = 0; i < 4; i++) {
						singleRow s = new singleRow(menu_name[i], images[i]);
						list.add(s);
					}
				}

				@Override
				public int getCount() {
					// TODO Auto-generated method stub
					return list.size();

				}

				@Override
				public Object getItem(int position) {
					// TODO Auto-generated method stub
					return list.get(position);

				}

				@Override
				public long getItemId(int position) {
					// TODO Auto-generated method stub
					return position;

				}

				@Override
				public View getView(int position, View convertView, ViewGroup parent) {
					// TODO Auto-generated method stub

					LayoutInflater inflater = (LayoutInflater) context
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View row = inflater.inflate(R.layout.single_row_menu, listMenu,
							false);

					TextView menu_name = (TextView) row.findViewById(R.id.tvMenu);
					ImageView image = (ImageView) row.findViewById(R.id.imMenu);

					singleRow temp = list.get(position);
					menu_name.setText(temp.menu_name);
					image.setImageResource(temp.image);
					return row;

				}

			}

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				// Toast.makeText(getApplicationContext(), "You clicked on position : "
				// + position + " and id : " + id, Toast.LENGTH_LONG).show();
				switch (position) {
				case 0:
					Intent detail= new Intent(this, UserDetail.class);
					startActivity(detail);
					break;
				case 1:
					Intent card= new Intent(this, CardInfo.class);
					startActivity(card);
					break;
				case 2:
					Intent score= new Intent(this, ScoreCollectActivity.class);
					startActivity(score);
					break;
				case 3:
					Toast.makeText(this, "Chức năng đang cập nhật", Toast.LENGTH_LONG).show();
					break;
				
				default:
					break;
				}

			}
}
