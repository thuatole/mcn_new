package com.example.mcn;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.aphidmobile.flip.FlipViewController;
import com.aphidmobile.flip.FlipViewController.ViewFlipListener;
import com.example.mcn.AppSettings.API;
import com.example.mcn.adpater.ListByCateAdapter;
import com.example.mcn.model.Category;
import com.example.mcn.model.Product;
import com.example.mcn.model.ProductList;
import com.example.utilities.AppSharedPreferenceUtil;
import com.example.utilities.PhoneUtil;
import com.google.gson.Gson;

public class ListByCate extends Activity {

	// Số item cần phân trang
	private static final int NUMBER_DOWNLOAD_ITEM = 5;

	private ActionBar actionBar;
	private FlipViewController flipView;
	private ProgressDialog mDialog;
	private Category category;
	private ListByCateAdapter adapter;
	private int total = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent i = getIntent();
		if (i != null) {
			if (i.getSerializableExtra("category") != null) {
				category = (Category) i.getSerializableExtra("category");
			}
		}
		flipView = new FlipViewController(this);
		// Use RGB_565 can reduce peak memory usage on large screen device, but
		// it's up to you to choose the best bitmap format
		flipView.setAnimationBitmapFormat(Bitmap.Config.RGB_565);

		adapter = new ListByCateAdapter(this);
		adapter.resetListByCate();
		flipView.setAdapter(adapter);

		setContentView(flipView);

		// Gọi api lấy dữ liệu product list
		callApiProductList(0, NUMBER_DOWNLOAD_ITEM);

		flipView.setOnViewFlipListener(new ViewFlipListener() {

			@Override
			public void onViewFlipped(View view, int position) {
				actionBar.setTitle(category.getName() + " (" + (position + 1)
						+ "/" + total + ")");
				if (position < total - 1 && position == adapter.getCount() - 1) {
					callApiProductList(position + 1, NUMBER_DOWNLOAD_ITEM);
				}
			}
		});

		actionBar = getActionBar();
		// actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setIcon(R.drawable.icon);
		actionBar.setHomeButtonEnabled(true);
		if (category.getTotal_products() > 0) {
			actionBar.setTitle(category.getName() + " (1/"
					+ category.getTotal_products() + ")");
		} else {
			actionBar.setTitle(category.getName() + " (0/0)");
		}
	}

	/**
	 * Gọi api api/category/products
	 * 
	 * @param offset
	 * @param quantity
	 */
	private void callApiProductList(int offset, int quantity) {
		showProcessDialog();
		AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
				this);
		// Get auth key
		String authKey = appPreference
				.getString(AppSettings.PREFERENCE_AUTH_KEY);

		HttpRequestApi reAPI = new HttpRequestApi(getApplicationContext(),
				API.DATA_PRODUCTS);
		reAPI.addParam("auth_key", authKey);
		reAPI.addParam("category_id", category.getId() + "");
		reAPI.addParam("offset", offset + "");
		reAPI.addParam("quantity", quantity + "");
		reAPI.execute(new HttpRequestListener<JSONObject>() {

			@Override
			public void onPostExecute(JSONObject result) {
				try {
					if (result != null) {
						Gson gson = new Gson();
						ProductList apiProductList = gson.fromJson(
								result.toString(), ProductList.class);
						if (apiProductList != null) {
							total = apiProductList.getTotal_product();
						}
						new DownloadTask().execute(apiProductList);
					} else {
						dismissProcessDialog();
					}
				} catch (Exception e) {
					dismissProcessDialog();
				}
			}

			@Override
			public void onPreExecute() {

			}

		});

	}
	
	/**
	 * Download dữ liệu
	 */
	private class DownloadTask extends
			AsyncTask<ProductList, Integer, List<Product>> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProcessDialog();
		}

		@Override
		protected List<Product> doInBackground(ProductList... params) {
			if (params[0] != null && params[0].getProducts() != null
					&& params[0].getProducts().size() > 0) {
				
				List<Product> productList = params[0].getProducts();
				
				for (int i = 0; i < productList.size(); i++) {
					if (productList.get(i) != null) {
						String urlImage = productList.get(i).getThumb();
						String name = urlImage.substring(urlImage
								.lastIndexOf("/") + 1);
						if (!PhoneUtil.hasExternalStoragePublicPicture(name)) {
							try {
								URL url = new URL(urlImage);
								HttpURLConnection connection = (HttpURLConnection) url
										.openConnection();
								int length = connection.getContentLength();
								InputStream is = (InputStream) url.getContent();
								byte[] imageData = new byte[length];
								int buffersize = (int) Math.ceil(length
										/ (double) 100);
								int downloaded = 0;
								int read;
								while (downloaded < length) {
									if (length < buffersize) {
										read = is.read(imageData, downloaded,
												length);
									} else if ((length - downloaded) <= buffersize) {
										read = is.read(imageData, downloaded,
												length - downloaded);
									} else {
										read = is.read(imageData, downloaded,
												buffersize);
									}
									downloaded += read;
								}
								Bitmap bitmap = BitmapFactory.decodeByteArray(
										imageData, 0, length);
								if (bitmap != null) {
									Log.i("TAG", "Bitmap created");
								} else {
									Log.i("TAG", "Bitmap not created");
								}
								is.close();

								if (bitmap != null) {
									PhoneUtil.saveToSDCard(bitmap, name, ListByCate.this);
								}
							} catch (Exception e) {
								Log.e("TAG",
										"Download image error: "
												+ e.getMessage());
								e.printStackTrace();
							}
						}
					}
				}
				adapter.addListByCate(productList);
			}

			return null;
		}

		@Override
		protected void onPostExecute(List<Product> result) {
			super.onPostExecute(result);
			adapter.notifyDataSetChanged();
			dismissProcessDialog();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		flipView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		flipView.onPause();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		// int id = item.getItemId();
		// if (id == R.id.action_settings) {
		// return true;
		// }
		switch (item.getItemId()) {
		case android.R.id.home:
			// onBackPressed();

			onBackPressed();
			return true;
		default:

			return super.onOptionsItemSelected(item);
		}
	}

	private void showProcessDialog() {
		if (mDialog == null)
			mDialog = new ProgressDialog(this);
		mDialog.setMessage("Loading...");
		mDialog.setCancelable(false);
		if (!mDialog.isShowing()) {
			mDialog.show();
		}
	}

	private void dismissProcessDialog() {
		if (mDialog == null)
			mDialog = new ProgressDialog(this);
		if (mDialog.isShowing()) {
			mDialog.dismiss();
		}
	}
}
