package com.example.mcn.model;

public class CommentSingle {

	private String name;
	private String date;
	private String score;
	private String content;
	
	public CommentSingle(String name, String date, String score, String content)
	{
		super();
		
		this.setName(name);
		this.setDate(date);
		this.setScore(score);
		this.setContent(content);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	
}
