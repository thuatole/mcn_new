package com.example.mcn.model;

public class ScoreInfo {

	private String time;
	private String detail;
	private String money_save;
	
	public ScoreInfo(String time, String detail, String money_save)
	{
		super();
		
		this.time = time;
		this.detail = detail;
		this.money_save = money_save;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getMoney_save() {
		return money_save;
	}

	public void setMoney_save(String money_save) {
		this.money_save = money_save;
	}
}
