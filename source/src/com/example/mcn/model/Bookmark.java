package com.example.mcn.model;

public class Bookmark {
	private String product_id;
	private String product_name;

	public Bookmark(String product_id, String product_name) {
		super();
		this.product_id = product_id;
		this.product_name = product_name;
	}

	public Bookmark() {

	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

}
