package com.example.mcn.model;

public class SearchList {

	private String name;
	private String id;
	
	public SearchList(String name, String id)
	{
		super();
		
		this.name = name;
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getID() {
		return id;
	}
	
	public void setID(String id) {
		this.id = id;
	}
}
