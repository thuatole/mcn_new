package com.example.mcn.model;

import java.io.Serializable;

public class Category implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String thumb;
	private String alias;
	private int total_products;

	public Category(int id, String name, String thumb, String alias,
			int total_products) {
		super();
		this.id = id;
		this.name = name;
		this.thumb = thumb;
		this.alias = alias;
		this.total_products = total_products;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getThumb() {
		return thumb;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public int getTotal_products() {
		return total_products;
	}

	public void setTotal_products(int total_products) {
		this.total_products = total_products;
	}
}
