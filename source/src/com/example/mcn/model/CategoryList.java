package com.example.mcn.model;

import java.util.List;

public class CategoryList {
	private int code;
	private List<Category> categories;

	public CategoryList() {

	}

	public CategoryList(int code, List<Category> categories) {
		super();
		this.code = code;
		this.categories = categories;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

}
