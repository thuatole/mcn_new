package com.example.mcn.model;

public class Product {
	private int id;
	private int id_pretty;
	private String name;
	private String phone;
	private String address;
	private String short_description;
	private String lat;
	private String lng;
	private Integer discount;
	private String thumb;

	public Product(int id, int id_pretty, String name, String phone,
			String address, String short_description, String lat, String lng,
			Integer discount, String thumb) {
		super();
		this.id = id;
		this.id_pretty = id_pretty;
		this.name = name;
		this.phone = phone;
		this.address = address;
		this.short_description = short_description;
		this.lat = lat;
		this.lng = lng;
		this.discount = discount;
		this.thumb = thumb;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_pretty() {
		return id_pretty;
	}

	public void setId_pretty(int id_pretty) {
		this.id_pretty = id_pretty;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getShort_description() {
		return short_description;
	}

	public void setShort_description(String short_description) {
		this.short_description = short_description;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public String getThumb() {
		return thumb;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}
}
