package com.example.mcn.model;

import java.util.List;

public class ProductList {
	private int code;
	private int total_product;
	private int offset;
	private int quantity;
	private List<Product> products;

	public ProductList(int code, int total_product, int offset, int quantity,
			List<Product> products) {
		super();
		this.code = code;
		this.total_product = total_product;
		this.offset = offset;
		this.quantity = quantity;
		this.products = products;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getTotal_product() {
		return total_product;
	}

	public void setTotal_product(int total_product) {
		this.total_product = total_product;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

}
