package com.example.mcn.model;

import static android.graphics.Bitmap.Config.ARGB_8888;

import java.io.InputStream;
import java.net.URL;
import java.text.NumberFormat;

import com.example.mcn.R;
//import com.squareup.picasso.Picasso;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

public class Deal {
	private String id;
	private String name;
	private String description;
	private int price;//dùng tạm cho percent conut down
	private int sell_price;
	private int sell_count;
	private String category_id;
	private String provider;
	private String content;
	private Bitmap img_bm;
	private String img;
	// set show hide button buy 1: show, 0: hide
	private int show_hide;
	// check deal save hay chưa khi load từ server
	private int is_save;
	// check lần đầu tiên load app
	private int is_load;
	// check list search results có đổ đầy màn hình hay ko
	private int is_search_full;

	private String dead_line;
	private double distance;
	private String address;
	private int dead_line_alert;
	private double lat, lng;
	private Context context;

	public Deal(String id, String name, String description, int price,
			int sell_price, int sell_count, String category_id,
			String provider, String content, Bitmap img, int is_save, int is_load,
			int show_hide, String dead_line) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.sell_price = sell_price;
		this.sell_count = sell_count;
		this.category_id = category_id;
		this.provider = provider;
		this.content = content;
		this.setImg_bm(img);
		this.is_save = is_save;
		this.show_hide = show_hide;
		this.is_load = is_load;
		this.dead_line = dead_line;
	}
	public Deal(String id, String name, String description, int price,
			int sell_price, int sell_count, String category_id,
			String provider, String content, String img, int is_save, int is_load,
			int show_hide, String dead_line) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.sell_price = sell_price;
		this.sell_count = sell_count;
		this.category_id = category_id;
		this.provider = provider;
		this.content = content;
		this.setImg(img);
		this.is_save = is_save;
		this.show_hide = show_hide;
		this.is_load = is_load;
		this.dead_line = dead_line;
	}
	
	public Deal(String id, String name, String description, int price,
			int sell_price, int sell_count, String category_id,
			String provider, String content, String img, int is_save, int is_load,
			int show_hide, String dead_line, Context context) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.sell_price = sell_price;
		this.sell_count = sell_count;
		this.category_id = category_id;
		this.provider = provider;
		this.content = content;
		this.setImg(img);
		this.is_save = is_save;
		this.show_hide = show_hide;
		this.is_load = is_load;
		this.dead_line = dead_line;
		this.context = context;
	}
	
	

	public Deal(String id, String name, String description, int price,
			int sell_price, int sell_count, String category_id,
			String provider, String content, Bitmap img, String dead_line, int dead_line_alert) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.sell_price = sell_price;
		this.sell_count = sell_count;
		this.category_id = category_id;
		this.provider = provider;
		this.content = content;
		this.setImg_bm(img);
		this.dead_line = dead_line;
		this.setDead_line_alert(dead_line_alert);

	}
	public Deal(String id, String name, String description, int price,
			int sell_price, int sell_count, String category_id,
			String provider, String content, String img, String dead_line, int dead_line_alert) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.sell_price = sell_price;
		this.sell_count = sell_count;
		this.category_id = category_id;
		this.provider = provider;
		this.content = content;
		this.setImg(img);
		this.dead_line = dead_line;
		this.setDead_line_alert(dead_line_alert);

	}

	public Deal(String id, String name, String description, int price,
			int sell_price, int sell_count, String category_id,
			String provider, String content, Bitmap img, int is_save, int is_load,
			int show_hide, int is_search_full, String dead_line) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.sell_price = sell_price;
		this.sell_count = sell_count;
		this.category_id = category_id;
		this.provider = provider;
		this.content = content;
		this.setImg_bm(img);
		this.is_save = is_save;
		this.show_hide = show_hide;
		this.is_load = is_load;
		this.is_search_full = is_search_full;
		this.dead_line = dead_line;
	}
	public Deal(String id, String name, String description, int price,
			int sell_price, int sell_count, String category_id,
			String provider, String content, String img, int is_save, int is_load,
			int show_hide, int is_search_full, String dead_line) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.sell_price = sell_price;
		this.sell_count = sell_count;
		this.category_id = category_id;
		this.provider = provider;
		this.content = content;
		this.setImg(img);
		this.is_save = is_save;
		this.show_hide = show_hide;
		this.is_load = is_load;
		this.is_search_full = is_search_full;
		this.dead_line = dead_line;
	}
	
	
	public Deal(String id, String name, String description, int price,
			int sell_price, int sell_count, String category_id,
			String provider, String content, Bitmap img, int is_save, int is_load,
			int show_hide, String dead_line, double distance, double lat, double lng, String address) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.sell_price = sell_price;
		this.sell_count = sell_count;
		this.category_id = category_id;
		this.provider = provider;
		this.content = content;
		this.setImg_bm(img);
		this.is_save = is_save;
		this.show_hide = show_hide;
		this.is_load = is_load;
		this.dead_line = dead_line;
		this.distance = distance;
		this.lat = lat;
		this.lng = lng;
		this.address = address;
	}
	public Deal(String id, String name, String description, int price,
			int sell_price, int sell_count, String category_id,
			String provider, String content, String img, int is_save, int is_load,
			int show_hide, String dead_line, double distance, double lat, double lng, String address) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.sell_price = sell_price;
		this.sell_count = sell_count;
		this.category_id = category_id;
		this.provider = provider;
		this.content = content;
		this.setImg(img);
		this.is_save = is_save;
		this.show_hide = show_hide;
		this.is_load = is_load;
		this.dead_line = dead_line;
		this.distance = distance;
		this.lat = lat;
		this.lng = lng;
		this.address = address;
	}
	public Deal(String id, String name, String description, int price,
			int sell_price, int sell_count, String category_id,
			String provider, String content, Bitmap img, int is_save, int is_load,
			int show_hide, String dead_line, double distance) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.sell_price = sell_price;
		this.sell_count = sell_count;
		this.category_id = category_id;
		this.provider = provider;
		this.content = content;
		this.setImg_bm(img);
		this.is_save = is_save;
		this.show_hide = show_hide;
		this.is_load = is_load;
		this.dead_line = dead_line;
		this.distance = distance;
	}
	public Deal(String id, String name, String description, int price,
			int sell_price, int sell_count, String category_id,
			String provider, String content, String img, int is_save, int is_load,
			int show_hide, String dead_line, double distance) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.sell_price = sell_price;
		this.sell_count = sell_count;
		this.category_id = category_id;
		this.provider = provider;
		this.content = content;
		this.setImg(img);
		this.is_save = is_save;
		this.show_hide = show_hide;
		this.is_load = is_load;
		this.dead_line = dead_line;
		this.distance = distance;
	}

	public Deal() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		Log.i("price", "price");
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		Log.i("price", "price");
		this.price = price;
	}

	public int getSell_price() {
		return sell_price;
	}

	public void setSell_price(int sell_price) {
		this.sell_price = sell_price;
	}

	public int getSell_count() {
		return sell_count;
	}

	public void setSell_count(int sell_count) {
		this.sell_count = sell_count;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	

	public int getShow_hide() {
		return show_hide;
	}

	public void setShow_hide(int show_hide) {
		this.show_hide = show_hide;
	}

	public int getIs_save() {
		return is_save;
	}

	public void setIs_save(int is_save) {
		this.is_save = is_save;
	}

	public int getIs_load() {
		return is_load;
	}

	public void setIs_load(int is_load) {
		this.is_load = is_load;
	}

	public int getIs_search_full() {
		return is_search_full;
	}

	public void setIs_search_full(int is_search_full) {
		this.is_search_full = is_search_full;
	}

	public String getDead_line() {
		return dead_line;
	}

	public void setDead_line(String dead_line) {
		this.dead_line = dead_line;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getDead_line_alert() {
		return dead_line_alert;
	}

	public void setDead_line_alert(int dead_line_alert) {
		this.dead_line_alert = dead_line_alert;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public Bitmap getImg_bm() {
		return img_bm;
	}

	public void setImg_bm(Bitmap img_bm) {
		this.img_bm = img_bm;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public void setBitmapFromImage(){
		NumberFormat defaultFormat = NumberFormat.getPercentInstance();
		defaultFormat.setMinimumFractionDigits(0);
		double sell_price_ = getSell_price();
		double price_ = getPrice();
		String price_percent = defaultFormat.format(sell_price_ / price_);
		
		Bitmap bitmap = null;
		try
		   {
			if(getImg().matches("null")){
				
			}
			else{
		       LoadImage task = new LoadImage();
		       task.execute(getImg());
		       bitmap = task.get();  //Add this
		       //Bitmap img = makeBitmap(getContext(), "- " + price_percent, bitmap);
				Bitmap img = bitmap;
				setImg_bm(img);
			}
		       
		   }
		   catch(Exception ex)
		   {
		       ex.printStackTrace();
		   }
		
		
	}
	
	private class LoadImage extends AsyncTask<String, String, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... args) {
			// TODO Auto-generated method stub
			Bitmap bitmap = null;
			try {
				 BitmapFactory.Options o = new BitmapFactory.Options();
			        o.inJustDecodeBounds = true;
			        o.inScaled = false;
			        o.inPreferredConfig = Bitmap.Config.RGB_565;
			         
			        BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent(), null, o);

			        //The new size we want to scale to
			        final int REQUIRED_SIZE=56;

			        //Find the correct scale value. It should be the power of 2.
			        int scale=1;
			        while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
			            scale*=2;

			        //Decode with inSampleSize
			        BitmapFactory.Options o2 = new BitmapFactory.Options();
			        o2.inSampleSize=scale;
			        o2.inScaled = false; 
			        o2.inPreferredConfig = Bitmap.Config.RGB_565;
		              bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent(), null, o2);
	               
	              
	              
	        } catch (Exception e) {
	              e.printStackTrace();
	        }
			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//bitmap = result;
			
			
		}
		//return bitmap;
		
			
	}
	
	public Bitmap makeBitmap(Context context, String text, Bitmap result) {
		Resources resources = getContext().getResources();
		float scale = resources.getDisplayMetrics().density;
		//Bitmap bitmap = BitmapFactory.decodeResource(resources,
			//	drawable.bgr_price);
		//bitmap = bitmap.copy(ARGB_8888, true);

		 //Bitmap bitmap_s = BitmapFactory.decodeResource(resources,
		 //drawable.deal_icon);

		/*
		 * Bitmap bitmap_s = null; try { bitmap_s =
		 * BitmapFactory.decodeStream(context
		 * .getContentResolver().openInputStream(uri));} catch
		 * (FileNotFoundException e) { //TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
		
		 
		Bitmap bitmap_1 = Bitmap.createScaledBitmap(result, 56, 56, true);
		
		bitmap_1 = bitmap_1.copy(ARGB_8888, true);
		Canvas canvas_ = new Canvas(bitmap_1);
		//Canvas canvas = new Canvas(bitmap);
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setColor(Color.WHITE); // Text color

		// paint.setShadowLayer(1f, 0f, 1f, Color.WHITE); // Text shadow

		Rect bounds = new Rect();
		paint.getTextBounds(text, 0, text.length(), bounds);

		int padding = 5;
		

		bounds.right += 2;

		Configuration config = getContext().getResources().getConfiguration();
		if (config.smallestScreenWidthDp > 320) {

			paint.setTextSize(4 * scale); // Text size
			bounds.bottom += padding * 2.5;
		} else {
			bounds.bottom += padding * 2;
			paint.setTextSize(5 * scale); // Text size
		}
		// canvas_.drawBitmap(bitmap, 0, 0, null);
		paint.setColor(Color.RED);
		paint.setStyle(Style.FILL);
		canvas_.drawRect(bounds, paint);
		paint.setColor(Color.WHITE);

		if (config.smallestScreenWidthDp > 320) {

			canvas_.drawText(text, padding - 2, padding + 5, paint);
		} else {

			canvas_.drawText(text, padding - 2, padding + 3, paint);
		}

		// Canvas canvas_ = new Canvas(bitmap_);

		return bitmap_1;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
	
	public String CalPercentPrice()
	{
		NumberFormat defaultFormat = NumberFormat.getPercentInstance();
		defaultFormat.setMinimumFractionDigits(0);
		double sell_price_ = getSell_price();
		double price_ = getPrice();
		String price_percent = defaultFormat.format(1-(sell_price_ / price_));
		return price_percent;
	}
	public void setImageDeal(Context context, ImageView image)
	{
//		if(getImg().matches("http://google.com")||getImg().matches("null")||getImg() == null)
//		{
//			image.setImageResource(R.drawable.logo_thumb);
//		}
//		else
//		{
			Picasso.with(context).load(getImg()).resize(80, 80)//resize đẩ giảm kích thước hình, trách tràn bộ  nhớ
			  .into(image); //url is image url
		//}
	}

}
