package com.example.mcn;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.mcn.AppSettings.API;
import com.example.mcn.adpater.SearchAdapter;
import com.example.mcn.model.SearchList;
import com.example.utilities.AppSharedPreferenceUtil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

public class SearchActivity extends Activity {
	
	private ProgressDialog progress;
	private ArrayList<SearchList> listSearch;
	private SearchAdapter searchAdapter;
	private ListView listViewSearch;
	private SearchView searchView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_view);
		listSearch = new ArrayList<SearchList>();
		listViewSearch = (ListView) findViewById(R.id.listSearch);
		listSearch.clear();
		searchAdapter = SearchAdapter.getInstance();
		searchAdapter.setContext(SearchActivity.this);
		
		listViewSearch.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Bundle bundle = new Bundle();
				SearchList clickItemSearch = listSearch.get(position);
				
				String idItem = clickItemSearch.getID();
				bundle.putString("product_id", idItem);
				Intent a = new Intent(SearchActivity.this, DetailProduct.class);
				a.putExtras(bundle);
				startActivity(a);				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);
        
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search_action)
				.getActionView();
        searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.onActionViewExpanded();
        searchView.setOnQueryTextListener(new OnQueryTextListener() {
			
			@Override
			public boolean onQueryTextSubmit(String query) {
				HttpRequestApi reAPI = new HttpRequestApi(getApplicationContext(),
						API.DATA_SEARCH);
				AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
						getApplicationContext());
				reAPI.addParam("auth_key",appPreference
						.getString(AppSettings.PREFERENCE_AUTH_KEY));
				reAPI.addParam("key", query);
				reAPI.addParam("offset", "0");
				reAPI.addParam("quantity", "100");
				reAPI.execute(new HttpRequestListener<JSONObject>() {
					
					@Override
					public void onPreExecute() {
						SearchActivity.this.progressDialog();
					}
					
					@Override
					public void onPostExecute(JSONObject result) {
						
						try {
							if(result != null) {
								JSONArray jsonDeals = result
										.getJSONArray("products");
								if (jsonDeals.length() > 0) {
									listSearch.clear();
									for (int i = 0; i < jsonDeals.length(); i++) {
										JSONObject object = jsonDeals
												.getJSONObject(i);
										String mID = object.getString("id");
										String mName = object.getString("name");

										SearchList searchInfo = new SearchList(
												mName, mID);
										listSearch.add(searchInfo);
									}
								}
							}
						} catch (JSONException e) {
							e.printStackTrace();
							SearchActivity.this.progress.dismiss();
						}
						
						SearchActivity.this.progress.dismiss();
						searchAdapter.setListScore(listSearch);
						listViewSearch.setAdapter(searchAdapter);
						searchAdapter.notifyDataSetChanged();
						
						// Hide keyboard
						searchView.clearFocus();
					}
				});
				

				return true;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				return false;
			}
		});
        
		return true;
	}
	
	protected void progressDialog()
	{
		ProgressBar pb = new ProgressBar(SearchActivity.this);
		
		pb.setIndeterminate(true);
		Drawable draw= getResources().getDrawable(R.drawable.customprogressbar);
		//pb.setProgressDrawable(draw);
		pb.setIndeterminateDrawable(draw);
		
		progress = new ProgressDialog(SearchActivity.this);
		
		progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		progress.show();
		progress.setContentView(pb);
	}
	
}
