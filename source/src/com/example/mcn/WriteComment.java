package com.example.mcn;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.mcn.AppSettings.API;
import com.example.mcn.model.CommentSingle;
import com.example.utilities.AppSharedPreferenceUtil;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class WriteComment extends Activity {

	private RatingBar scoreSeekBar;
	private TextView scoreBySeekBar, tvName, tvAdd;
	private EditText content;
	private String  product_id, address, name;
	private int score = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.write_comment);
		scoreSeekBar  = (RatingBar)findViewById(R.id.seekBar);
		scoreBySeekBar = (TextView)findViewById(R.id.scoreBySeekBar);
		tvName = (TextView)findViewById(R.id.tvName);
		tvAdd = (TextView)findViewById(R.id.tvAdd);
		content = (EditText)findViewById(R.id.etContentCmt);
		
		Bundle gotDealId = getIntent().getExtras();
		product_id = gotDealId.getString("product_id");
		Bundle gotName = getIntent().getExtras();
		name = gotName.getString("name");
		Bundle gotAdd = getIntent().getExtras();
		address = gotAdd.getString("address");
		
		
		tvName.setText(name);
		tvAdd.setText(address);
		score = (int)scoreSeekBar.getRating();
		

		scoreSeekBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			public void onRatingChanged(RatingBar ratingBar, float rating,
				boolean fromUser) {
	 
				//txtRatingValue.setText(String.valueOf(rating));
				score = (int)rating;
	 
			}
		});

		
		
	}

//	@Override
//	public void onProgressChanged(SeekBar seekBar, int progress,
//			boolean fromUser) {
//		// TODO Auto-generated method stub
//		score = progress;
//		scoreBySeekBar.setText(progress + ".0");
//		
//	}
//
//	@Override
//	public void onStartTrackingTouch(SeekBar seekBar) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void onStopTrackingTouch(SeekBar seekBar) {
//		// TODO Auto-generated method stub
//		
//	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();

			return true;
		case R.id.action_report:

			if(scoreSeekBar.getProgress() != 0)
			{
			AppSharedPreferenceUtil appPreference = new AppSharedPreferenceUtil(
					WriteComment.this);
			HttpRequestApi reAPI = new HttpRequestApi(
					getApplicationContext(), API.POST_COMMENT);
			reAPI.addParam("product_id", product_id);
			reAPI.addParam("mark", score +"");
			reAPI.addParam("body", content.getText().toString());
			reAPI.addParam("auth_key",appPreference
					.getString(AppSettings.PREFERENCE_AUTH_KEY));
			String auth  = appPreference
					.getString(AppSettings.PREFERENCE_AUTH_KEY);
			
			reAPI.execute(new HttpRequestListener<JSONObject>() {

				// HÃ m nÃ y sáº½ thá»±c thi trÆ°á»›c
				@Override
				public void onPreExecute() {
					// TODO Auto-generated method stub

				}

				// HÃ m nÃ y nháº­n ket qua tráº£ zÃ¬a
				@Override
				public void onPostExecute(JSONObject result) {
					// TODO Auto-generated method stub

					try {
						if (result.getInt("code") == 0) {

							
							onBackPressed(); 

						}// end if code ==1
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			});// end api
			}
			else
			{
				Toast.makeText(this, "Vui lòng chọn điểm lớn hơn 0", Toast.LENGTH_LONG).show();
			}
			
			
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_report, menu);

		return super.onCreateOptionsMenu(menu);
	}
}
