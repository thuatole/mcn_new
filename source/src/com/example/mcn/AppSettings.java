package com.example.mcn;

import android.app.ActionBar.Tab;
import android.graphics.Bitmap;
import android.os.Environment;

public class AppSettings {
	public static final String SECRET_KEY = "19be792cfdab74abaa1cf1a8b4f6a69a";
	
	//settings
	public static String DEVICE_AUTH_KEY;
	
	
	
	public static final String CTG_CA = "cong_an";
	public static final String CTG_TN = "tai_nan";
	public static final String CTG_KX = "ket_xe";

	public static final String AUTH_KEY = "AUTH_KEY";
	public static final String SERVICE_START = "SERVICE_START";
	
	public static final String REG_DEVICE = "REG_DEVICE";
	public static final String IN_RANGE = "IN_RANGE";
	public static final String TIME_CA_CLICK = "TIME_CA_CLICK";
	public static final String TIME_TN_CLICK = "TIME_TN_CLICK";
	public static final String TIME_KX_CLICK = "TIME_KX_CLICK";
	
	public static final String MODE_VIBRATION = "MODE_VIBRATION";
	public static final String MODE_RINGTONE = "MODE_RINGTONE";

	public static final int SPLASH_TIME = 3000;
	//CHECK SAVE BTN IN DETAIL PAGE IS PRESSED
	public static int CHECK_UPDATE_INFO = 0;
	public static int FIRST_LOAD = 0;
	public static int CHECK_SPLASH_SPEED = 0;
	public static int CHECK_SAVE_BTN = 0;
	public static int CHECK_SAVE_BTN_SEARCH = 0;
	public static int CHECK_DELETE_BTN = 0;
	public static int CHECK_SEARCH_FULL = 0;
	public static int CHECK_SAVE_BTN_MAP = 0;
	public static int CHECK_SAVE_BTN_NEWDEAL = 0;
	public static int CHECK_SAVE_BTN_NEARDEAL = 0;
	public static int CURRENT_PAGE_POSITION = 0;
	public static int check_pull_to_refresh = 0;
	public static boolean CHECK_CONNECTION = true;
	public static boolean CHECK_AD_GOOGLE = false;//'check xem đã remove dc ad google chư, để refresh lại main activity
	public static boolean CHECK_RESUME_FROM_SEARCH = false;
	public static final String ACCESS_KEY = "k7873i8vas52sflw5q3u";
	public static final String SECRECT_KEY = "wh4n83pxil7fetxgpw3l3tpiy879569o";
	public static final int TIME_PUT_NODE = 120000;

	
	public static final String API_SERVER = "http://mcn.vn:7782/";
	public static final String API_REG_DEVICE = "api/device/login";
	//public static final String API_REG_DEVICE = "api/deals";
	public static final String API_DATA_NEAR = "api/product/near";
	public static final String API_DATA_DETAIL = "api/product/detail";
	public static final String API_DATA_USER = "api/device/update-info";
	public static final String API_CARD_INFO = "api/device/card-info";

	public static final String API_INIT_APP = "api/device";
	public static final String API_DATA_CATEGORY_LIST = "api/category/list";
	public static final String API_DATA_PRODUCTS = "api/category/products";
	public static final String API_DATA_MONTH = "api/card/month-analysis";
	public static final String API_DATA_SEARCH = "/api/product/search";
	
	public static final String API_BARCODE = "/api/card/pay";
	public static final String API_GET_COMMENT = "/api/product/get-comments";
	public static final String API_POST_COMMENT = "/api/product/add-comment";
	public static final String API_CARD_RENEW = "/api/card/renew";
	// SharedPreference Key
	public static final String PREFERENCE_AUTH_KEY = "PREFERENCE_AUTH_KEY";
	
	// Image folder
	public static final String FOLDER_IMAGE_APP = "MCN";
	
	public static Tab selectTab;
	public static Bitmap bitmap;
	
	// Flag is share facebook
	public static boolean IS_SHARE = false;
	
	public static String PATH_IMAGE_SDCARD = Environment.getExternalStorageDirectory().toString()
			+ "/" + AppSettings.FOLDER_IMAGE_APP + "/";
	
	public enum API {

		REG_DEVICE, DATA_NEAR, DATA_DETAIL, DATA_CATEGORY_LIST, DATA_BARCODE, DATA_USER, CARD_INFO, DATA_PRODUCTS, DATA_MONTH, DATA_SEARCH, GET_COMMENT, POST_COMMENT,
		CARD_RENEW

	}
}
