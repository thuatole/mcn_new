package com.example.utilities;

import java.util.List;

import com.example.utilities.NetworkUtil;

import android.R;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import  com.example.mcn.AppSettings;
import com.example.mcn.CheckConnection;


public class NetworkChangeReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		 boolean status = NetworkUtil.getConnectivityStatusString(context);
		 
	        //Toast.makeText(context, status+"", Toast.LENGTH_LONG).show();
	        AppSettings.CHECK_CONNECTION = status;
	        if(status == false){
	        	ActivityManager activityManager = (ActivityManager) context
	        			.getSystemService(Context.ACTIVITY_SERVICE);
	        			    List<RunningTaskInfo> services = activityManager
	        			.getRunningTasks(Integer.MAX_VALUE);
	        			    if (services.get(0).topActivity.getPackageName().toString()
	        			.equalsIgnoreCase(context.getPackageName().toString())) {
	        			    	//Toast.makeText(context, "Không tìm thấy kết nối internet, ứng dụng sẽ tự đóng !"+ AppSettings.CHECK_CONNECTION, Toast.LENGTH_LONG).show();
				            	Intent mIntent = new Intent(context, CheckConnection.class);
								mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
										| Intent.FLAG_ACTIVITY_NEW_TASK);
								context.startActivity(mIntent);
	        			    	
	        			    }
	        			    
	        			    //Toast.makeText(context, "Không tìm thấy kết nối internet, ứng dụng sẽ tự đóng !"+ AppSettings.CHECK_CONNECTION, Toast.LENGTH_LONG).show();
//	        			    new AlertDialog.Builder(context)
//	        			    .setTitle("Thông báo")
//	        			    .setMessage("Không tìm thấy kết nối internet, ứng dụng MCN sẽ tự đóng !")
//	        			    .setPositiveButton("Đóng", new DialogInterface.OnClickListener() {
//	        			        public void onClick(DialogInterface dialog, int which) { 
//	        			            // continue with delete
//	        			        	 System.exit(0);
//	        			        }
//	        			     })
//	        			     .show();
	        			    
	        			   
	        }
	        
	}
}
