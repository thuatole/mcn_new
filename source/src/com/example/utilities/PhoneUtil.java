package com.example.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import com.example.mcn.AppSettings;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;

public class PhoneUtil {

	/*
	 * get DeviceId
	 * Params from fragment, activity: getBaseContext(), getContentResolver() 
	 */
	public static String getDeviceId(Context context, ContentResolver contentResolver){
    	final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(contentResolver, android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();
        Log.i("device-id", "tmDevice" + tmDevice);
        Log.i("device-id", deviceId);
        Log.i("device-id", "tmSerial: " + tmSerial);
        Log.i("device-id", "androidId: " + androidId);
        return deviceId;
    }
	
	public static String osModel(){
		return android.os.Build.MODEL;
	}
	
	/**
	 * Lưu image vào SDCard
	 * 
	 * @param bitmap
	 * @param name
	 */
	public static void saveToSDCard(Bitmap bitmap, String name, Context context) {
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			mExternalStorageAvailable = mExternalStorageWriteable = true;
			Log.v("TAG", "SD Card is available for read and write "
					+ mExternalStorageAvailable + mExternalStorageWriteable);
			saveFile(bitmap, name, context);
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
			Log.v("TAG", "SD Card is available for read "
					+ mExternalStorageAvailable);
		} else {
			mExternalStorageAvailable = mExternalStorageWriteable = false;
			Log.v("TAG", "Please insert a SD Card to save your Video "
					+ mExternalStorageAvailable + mExternalStorageWriteable);
		}
	}

	/**
	 * Lưu file
	 * 
	 * @param bitmap
	 * @param name
	 */
	public static void saveFile(Bitmap bitmap, String name, Context context) {
		String filename = name;
		ContentValues values = new ContentValues();
		File sdImageMainDirectory = new File(
				Environment.getExternalStorageDirectory(),
				AppSettings.FOLDER_IMAGE_APP);
		sdImageMainDirectory.mkdirs();
		File outputFile = new File(sdImageMainDirectory, filename);
		values.put(MediaStore.MediaColumns.DATA, outputFile.toString());
		values.put(MediaStore.MediaColumns.TITLE, filename);
		values.put(MediaStore.MediaColumns.DATE_ADDED,
				System.currentTimeMillis());
		values.put(MediaStore.MediaColumns.MIME_TYPE, "image/png");
		Uri uri = context.getContentResolver().insert(
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,

				values);
		try {
			OutputStream outStream = context.getContentResolver()
					.openOutputStream(uri);
			bitmap.compress(Bitmap.CompressFormat.PNG, 95, outStream);

			outStream.flush();
			outStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Kiểm tra tồn tại file
	 * 
	 * @param name
	 * @return
	 */
	public static boolean hasExternalStoragePublicPicture(String name) {
		File sdImageMainDirectory = new File(
				Environment.getExternalStorageDirectory(),
				AppSettings.FOLDER_IMAGE_APP);
		File file = new File(sdImageMainDirectory, name);
		return file.exists();
	}
	
	public static void deleteImage(String name) {
		try {
			File sdImageMainDirectory = new File(
					Environment.getExternalStorageDirectory(),
					AppSettings.FOLDER_IMAGE_APP);
			File file = new File(sdImageMainDirectory, name);
			if (file != null) {
				file.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
