package com.example.utilities;

import android.content.Context;
import android.content.SharedPreferences;

public class AppSharedPreferenceUtil {
	private final SharedPreferences preferences;
	private final String DefPreferenceName = "mcn_reference";
	private SharedPreferences.Editor editor;

	public AppSharedPreferenceUtil(Context context) {
		this.preferences = context.getSharedPreferences(DefPreferenceName,
				Context.MODE_PRIVATE);
	}

	public void putString(String key, String value) {
		if (editor != null) {
			editor.putString(key, value);
		}
		if (value == null) {
			preferences.edit().remove(key).commit();
		} else {
			preferences.edit().putString(key, value).commit();
		}
	}

	public String getString(String key) {
		if (preferences.contains(key)) {
			return preferences.getString(key, "");
		}
		return null;
	}

	public void removeByKey(String key) {
		if (key != null && key != "" && preferences.contains(key)) {
			preferences.edit().remove(key).commit();
		}
	}

	public boolean containsKey(String key) {
		return preferences.contains(key);
	}

	public void clear() {
		preferences.edit().clear().commit();
	}
}
