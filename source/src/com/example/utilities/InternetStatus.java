package com.example.utilities;

import android.content.Context;

public interface InternetStatus {
	public void isConnectingToInternet(Context context);
}
